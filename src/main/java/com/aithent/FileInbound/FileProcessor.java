package com.aithent.FileInbound;



import java.io.File;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.file.GenericFile;

public class FileProcessor   implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
	System.out.println("*************** File Processor body**********");
	
	GenericFile<?> input = (GenericFile<?>)exchange.getIn().getBody();
	
	 
	 System.out.println("File Name =============>"+input.getFileName());
	 
  File file =	 (File) input.getFile();
  
  System.out.println("File Obj Name =============>"+file.getName());
		
	}

}
