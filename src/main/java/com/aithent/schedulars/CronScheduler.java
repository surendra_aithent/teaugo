package com.aithent.schedulars;

/*@Author Surendra.Mitta 26June2017
*/

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import ca.uhn.hl7v2.model.Message;

import com.aithent.Messages.MessageCounter;
import com.velos.integration.adt.dao.ADTDao;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.processor.MessageProcess;

@Component
public class CronScheduler extends MessageProcess {
	private static Logger logger = Logger.getLogger(CronScheduler.class);


	private MessageCounter messageCounter;


  public MessageCounter getMessageCounter() {
		return messageCounter;
	}


	public void setMessageCounter(MessageCounter messageCounter) {
		this.messageCounter = messageCounter;
	}


public void run() throws Exception {
	  messageCounter.processedMsgCounter();
  }
	
}