package com.aithent.Messages;
/*@Author Surendra.Mitta 26June2017
*/
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.velos.integration.adt.dao.ADTDao;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.processor.MessageProcess;

public class MessageCounter {
	
	private static Logger logger = Logger.getLogger(MessageCounter.class);
	private MessageDao messageDao;
	Map<String,Object> msgCntMap = null;
	//Properties  prop = new Properties();

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	
	private Date yesterday() {
	    final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    return cal.getTime();
	}
public void processedMsgCounter(){
	msgCntMap = new HashMap<String,Object>();
	try{
		
		logger.info("*****Message Count Starts*****");
	msgCntMap=messageDao.messageCount();
	logger.info("*****Message Count Ends*****");
	
	}catch(Exception e){
		e.printStackTrace();
	}
	
}

}
