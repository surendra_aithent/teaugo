
package com.velos.integration.init;

	import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

	import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.velos.integration.core.messaging.PatientOutboundClientThread;

	public class PatientAppServletContextListener implements ServletContextListener {

		private static Logger logger = Logger.getLogger("epicLogger");
		
		private ApplicationContext context;

		@Override
		public void contextDestroyed(ServletContextEvent arg0) {}

		@Override
		public void contextInitialized(ServletContextEvent arg0) {
			logger.info("Initiating PatientAppServletContextListener Core");
			outBoundMessaging();
		}
		
		void outBoundMessaging() {
			Properties prop = new Properties();
			
			try {
				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
			} catch (IOException e1) {
				logger.error("***config.properties file missmatched***");		}
			if(prop.containsKey("enable_Epic_Interface") == true && prop.getProperty("enable_Epic_Interface").equalsIgnoreCase("y")
					&& !"".equals(prop.getProperty("enable_Epic_Interface"))){
		
			(new Thread(new PatientOutboundClientThread())).start();
			}
		}

	}
