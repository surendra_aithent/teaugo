package com.velos.integration.hl7.util;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Properties;

public class PropertyFileManager {
	
  public static void main(String[] args) {
	  String filePath = "/home/sbalaji/home_ext/workspace/eResForms_WorkSpace/PropertyFileManagement/src/config.properties";
	  HashMap<String, String> settingsMap = new HashMap<String, String>();
	  settingsMap.put("A", "ON");
	  settingsMap.put("B", "ON");
	  updatePropertFile(filePath,settingsMap);
	  
	
  }
  	 public void getProperties(HashMap<String,String> resultMap){
  		 
  		ClassLoader classLoader = getClass().getClassLoader();
  		
  		File file = new File(classLoader.getResource("config.properties").getFile());
  		System.out.println("FilePath =="+file);
  		
  		updatePropertFile(file.getAbsolutePath(),resultMap);
  		
  	}

private static void updatePropertFile(String filePath,HashMap<String,String> settingsMap) {
		// TODO Auto-generated method stub
		FileInputStream in = null;
		FileOutputStream out = null;
		Properties props = new Properties();
		OutputStream output = null;
		
		try {
	
			in = new FileInputStream(filePath);
			props.load(in);
			in.close();
	
			out = new FileOutputStream(filePath);
			System.out.println("upd");
			for (Object prefFromClientKey : settingsMap.keySet()) {
				System.out.println("upd1");
				props.setProperty((String)prefFromClientKey, settingsMap.get(prefFromClientKey));
				System.out.println((String)prefFromClientKey+" "+settingsMap.get(prefFromClientKey));
			}
			props.store(out, null);
			System.out.println("upd");
			//out.close();
	
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
		}
	}
}



/*props.setProperty("Comapny name", "Aithent USA");
props.setProperty("Name", "Balaji1");
props.setProperty("Name", "Balaji2");
props.setProperty("Name", "Balaji3");*/