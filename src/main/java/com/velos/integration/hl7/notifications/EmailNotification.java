package com.velos.integration.hl7.notifications;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.apache.log4j.Logger;

public class EmailNotification {
	private static Logger logger = Logger
			.getLogger(EmailNotification.class.getName());
	
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	private SimpleMailMessage smMessage;
	Properties prop = new Properties();
	InputStream input,input1;
	

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public SimpleMailMessage getSmMessage() {
		return smMessage;
	}

	public void setSmMessage(SimpleMailMessage smMessage) {
		this.smMessage = smMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	//Error Email Notification
	public void sendMail(String content,String errorMsg,String studyCoordinators) throws IOException, HL7CustomException {
		logger.info("Error Email Notification");
		
		MimeMessage errormessage=mailSender.createMimeMessage();
		List<String> list = null;
		
		
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			logger.info("Message Parameters set");
			String enviornment = prop.getProperty("enviornment");  
			logger.info("Message Parameters set1");
			MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
			logger.info("Message Parameters set2");
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(smMessage.getFrom());
				}
			String to [] = new String[100];
			to =smMessage.getTo();

	
	if(studyCoordinators != null || !"".equals(studyCoordinators)){
		 list = new ArrayList<String>(Arrays.asList(to));
		 
		for(String a :studyCoordinators.split(",")){
		list.add(a);
		}
	     String emailId[]=new String[list.size()];
	     
	     for(int i=0; i<list.size();i++){
	    	 emailId[i] = list.get(i);
	     }
	     

			helper.setTo(emailId);
	}else{
		helper.setTo(to);
	}
			
			logger.info("Message Parameters set3");
			helper.setSubject(enviornment + " : "+ smMessage.getSubject());
			logger.info("Message Parameters set4");
			
			helper.setText(String.format(
					smMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody3").toString().trim()+"\n\n"
							+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set5");
			
			mailSender.send(errormessage);
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),"AE");
		}
		
		
	}
	
	public void sendMail(String content,String errorMsg) throws IOException, HL7CustomException {
		logger.info("Error Email Notification");
		
		MimeMessage errormessage=mailSender.createMimeMessage();
		
		
		
		try{
			input=this.getClass().getClassLoader().getResourceAsStream("email.properties");
			prop.load(input);
			logger.info("Message Parameters set");
			String enviornment = prop.getProperty("enviornment");  
			logger.info("Message Parameters set1");
			MimeMessageHelper helper = new MimeMessageHelper(errormessage, true);
			logger.info("Message Parameters set2");
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(smMessage.getFrom());
				}
			//String to [] = smMessage.getTo();
			
			helper.setTo(smMessage.getTo());
			
			logger.info("Message Parameters set3");
			helper.setSubject(enviornment + " : "+ smMessage.getSubject());
			logger.info("Message Parameters set4");
			
			helper.setText(String.format(
					smMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody3").toString().trim()+"\n\n"
							+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set5");
			
			mailSender.send(errormessage);
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),"AE");
		}
		
		
	}
	
	
	
	//Success Email Notification
		public void sendMail(String content,String errorMsg,String msgdate,String mrn,String msgId) throws IOException, HL7CustomException {
			logger.info("Creating Mime Message");
			MimeMessage message = mailSender.createMimeMessage();
			logger.info("Mime Message got created");
			

			try{
				
				input1 = this.getClass().getClassLoader()
						.getResourceAsStream("email.properties");			
				prop.load(input1);
				
				logger.info("Setting message Parameters");
				String enviornment = prop.getProperty("enviornment");   
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				
				System.out.println("Message Id--->"+msgId);
				
				//helper.setFrom(simpleMailMessage.getFrom());
				if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
					helper.setFrom(simpleMailMessage.getFrom());
					}
				helper.setTo(simpleMailMessage.getTo());
			
				helper.setSubject(enviornment + " : " 
						+ simpleMailMessage.getSubject() +mrn+" } - Message Id "+"{ "+msgId+" }");
				
				helper.setText(String.format(
						simpleMailMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
								+ content+"\n\n\n"+errorMsg));		
				logger.info("Message Parameters set");
				/*
				String fileLoc = new File(System.getProperty("catalina.base"))+ "/logs/coverage.log";
				logger.info("Log File location : " + fileLoc);
				File file = new File(fileLoc);			
				helper.addAttachment("coverage.log", file);*/
				mailSender.send(message);
				logger.info("Message sent");
			}catch (Exception e) {
				e.printStackTrace();
				throw new HL7CustomException(e.getMessage(),"AE");
			}finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}
	
	
	//Success Email Notification
	public void sendMail(String content,String errorMsg,String msgdate,String mrn,String msgId,String studyCoordinators) throws IOException, HL7CustomException {
		logger.info("Creating Mime Message");
		MimeMessage message = mailSender.createMimeMessage();
		logger.info("Mime Message got created");
		System.out.println("Sending email Notification");
		List<String> list = null;

		try{
			
			input1 = this.getClass().getClassLoader()
					.getResourceAsStream("email.properties");			
			prop.load(input1);
			
			logger.info("Setting message Parameters");
			String enviornment = prop.getProperty("enviornment");   
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			
			System.out.println("Message Id--->"+msgId);
			
			//helper.setFrom(simpleMailMessage.getFrom());
			if(!"".equals(prop.getProperty("mail.from")) && prop.containsKey("mail.from")){
				helper.setFrom(simpleMailMessage.getFrom());
				}
			//helper.setTo(simpleMailMessage.getTo());
			String to [] = new String[100];
					to =simpleMailMessage.getTo();
					System.out.println("Email Id 1 ===>"+to[0]);
					System.out.println("studyCoordinators Object =======>"+studyCoordinators);
			
			if(studyCoordinators == null ||"".equals(studyCoordinators) ){
				helper.setTo(to);
			}else if(!"".equals(studyCoordinators)||studyCoordinators != null ){
				 list = new ArrayList<String>(Arrays.asList(to));
				 
				for(String a :studyCoordinators.split(",")){
				list.add(a);
				}
			     String emailId[]=new String[list.size()];
			     
			     for(int i=0; i<list.size();i++){
			    	 emailId[i] = list.get(i);
			     }
			     

					helper.setTo(emailId);
			}
			
			helper.setSubject(enviornment + " : " 
					+ simpleMailMessage.getSubject() +mrn+" } - Message Id "+"{ "+msgId+" }");
			
			helper.setText(String.format(
					simpleMailMessage.getText(),"","\n"+ prop.getProperty("mail.msgbody").toString().trim()+"\n\n"
							+ content+"\n\n\n"+errorMsg));		
			logger.info("Message Parameters set");
			/*
			String fileLoc = new File(System.getProperty("catalina.base"))+ "/logs/coverage.log";
			logger.info("Log File location : " + fileLoc);
			File file = new File(fileLoc);			
			helper.addAttachment("coverage.log", file);*/
			mailSender.send(message);
			logger.info("Message sent");
		}catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getCause().getMessage(),"AE");
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
