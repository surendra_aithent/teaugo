package com.velos.integration.hl7.bean;

public class ResponseMessageBean {
	
	private String messageType;
	private String segmentName;
	private String fieldName;
	private String terserPath;
	private int fieldFrom;
	private String fieldValue;
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getTerserPath() {
		return terserPath;
	}
	public void setTerserPath(String terserPath) {
		this.terserPath = terserPath;
	}
	
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public int getFieldFrom() {
		return fieldFrom;
	}
	public void setFieldFrom(int fieldFrom) {
		this.fieldFrom = fieldFrom;
	}
	

}
