package com.velos.integration.hl7.bean;

public class MessageFieldBean {
	
	private String messageType; 
	private String segmentName;

	private String fieldName; 
	private String fieldFunction;
	private String	columnName ;
	private  int    isMandatory ;
	private String	repeated ;
	private String	repeatedColumn;
	private String	repeatedDelimiter ;
	private String	mappingType;
	private String	mappingField;
	private int maxLength ;
	private String terserPath;
	private String client;
	
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String message_type) {
		this.messageType = message_type;
	}
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segment_name) {
		this.segmentName = segment_name;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String field_name) {
		this.fieldName = field_name;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumn_name(String column_name) {
		this.columnName = column_name;
	}
	public int getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(int is_mandatory) {
		this.isMandatory = is_mandatory;
	}
	public String getRepeated() {
		return repeated;
	}
	public void setRepeated(String repeated) {
		this.repeated = repeated;
	}
	public String getRepeatedColumn() {
		return repeatedColumn;
	}
	public void setRepeatedColumn(String repeated_column) {
		this.repeatedColumn = repeated_column;
	}
	public String getRepeatedDelimiter() {
		return repeatedDelimiter;
	}
	public void setRepeatedDelimiter(String repeated_delimiter) {
		this.repeatedDelimiter = repeated_delimiter;
	}
	public String getMappingType() {
		return mappingType;
	}
	public void setMappingType(String mapping_type) {
		this.mappingType = mapping_type;
	}
	public String getMappingField() {
		return mappingField;
	}
	public void setMappingField(String mapping_field) {
		this.mappingField = mapping_field;
	}
	public int getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(int max_length) {
		this.maxLength = max_length;
	}
	public String getTerserPath() {
		return terserPath;
	}
	public void setTerserPath(String terserPath) {
		this.terserPath = terserPath;
	}

}
