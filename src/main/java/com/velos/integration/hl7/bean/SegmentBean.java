package com.velos.integration.hl7.bean;

public class SegmentBean {
	
	private String messageType;
	private String segmentName;
	private int isMandatory;
	private String repeated;
	private String repeatedSegmentTable;
	private int segmentSequence;
	private int segmentType;
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	public int getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(int isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getRepeated() {
		return repeated;
	}
	public void setRepeated(String repeated) {
		this.repeated = repeated;
	}
	public String getRepeatedSegmentTable() {
		return repeatedSegmentTable;
	}
	public void setRepeatedSegmentTable(String repeatedSegmentTable) {
		this.repeatedSegmentTable = repeatedSegmentTable;
	}
	public int getSegmentSequence() {
		return segmentSequence;
	}
	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}
	public int getSegmentType() {
		return segmentType;
	}
	public void setSegmentType(int segmentType) {
		this.segmentType = segmentType;
	}
	
	

}
