package com.velos.integration.hl7.processor;


import java.util.List;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

import com.velos.integration.hl7.bean.MessageBean;
import com.velos.integration.hl7.bean.MessageFieldBean;
import com.velos.integration.hl7.bean.SegmentBean;
import com.velos.integration.hl7.dao.MessageConfiguration;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.util.MessageValidationUtil;

public class Hl7Processor implements Processor {

	private static Logger logger = Logger.getLogger(Hl7Processor.class);

	private MessageProcess messageProcess;
	private MessageValidationUtil validationUtil;
	private MessageConfiguration messageConfiguration;
	private MessageDao messageDao;
	private MessageProcessFactory messageProcessFactory;
	Properties prop=null;
	Properties prop1 =null;

	public MessageProcessFactory getMessageProcessFactory() {
		return messageProcessFactory;
	}

	public void setMessageProcessFactory(MessageProcessFactory messageProcessFactory) {
		this.messageProcessFactory = messageProcessFactory;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public void setValidationUtil(MessageValidationUtil validationUtil) {
		this.validationUtil = validationUtil;
	}
	public MessageConfiguration getMessageConfiguration() {
		return messageConfiguration;
	}

	public void setMessageConfiguration(MessageConfiguration messageConfiguration) {
		this.messageConfiguration = messageConfiguration;
	}

	public MessageProcess getMessageProcess() {
		return messageProcess;
	}

	public void setMessageProcess(MessageProcess messageProcess) {
		this.messageProcess = messageProcess;
	}


	@Override
	public void process(Exchange exchange) throws Exception {
		prop1 = new Properties();
		logger.info("*** Checking ADT Interface Enable / Disable ***");
		try {
			prop1.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			logger.info("Config.Properties file doesn't Exist");
			e.printStackTrace();
			throw e;
		}				

		if(prop1.containsKey("enable_ADT_Interface") == true && prop1.getProperty("enable_ADT_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop1.getProperty("enable_ADT_Interface"))){
			logger.info("*** ADT INTERFACE ENABLED ***");
			prop=new Properties();
			String messageString = exchange.getIn().getBody(String.class);
			prop1.load(getClass().getClassLoader().getResourceAsStream("ackcodes.properties"));

			logger.info("\nIncoming Message ::\n"+messageString);



			Message output = null;
			Message input=null; 
			String messageType=null;
			try{

				// Message Pre-Process
				//Commented By Aman  
				//mpreprocess.preProcess(messageString);

				//List<String> segList=mpreprocess.segmentList(messageString);
				PipeParser pipeParser = new PipeParser();
				pipeParser.setValidationContext(new HL7ValidationContext());
				input = pipeParser.parse(messageString);

				Terser t=new Terser(input);

				messageType=t.get("/MSH-9-1");
				System.out.println("*********After Pipe parser********");
				messageProcess = messageProcessFactory.getMessageProcessInstance(messageProcess, messageType);
				logger.info("messageProcess=="+messageProcess);
				//Validation Starts
				List<MessageBean> messageBeanList = messageConfiguration.getMessageConfigurationBean();
				List<SegmentBean> segmentBeanList = messageConfiguration.getSegmentConfigurationBean();
				List<MessageFieldBean> messageFieldBeanList = messageConfiguration.getMessageFieldsConfiguration();
				validationUtil.validateMessage(messageString,messageType,messageBeanList,messageFieldBeanList, segmentBeanList);
				//Validation ends

				//Based on message type, load the Message Type configurations. & Message Processing Starts
				output=messageProcess.processMessage(input);
				if(output!=null){
					messageDao.insertMessageAudit(messageType,input,output);
					exchange.getOut().setBody(output);
				}
				//inputStream.close();
				//Catching Exceptions from all Classes
			} catch (HL7CustomException e) {
				logger.error("\nError while parsing the incoming message :: \n");
				e.printStackTrace();
				String emailNotfMsg=e.getEmailNotfMsg();
				String errMessage=null;
				errMessage=e.getLocalizedMessage();
				logger.info("emailNotfMsg ----->"+emailNotfMsg);
				logger.error("HL7 Error Message --->"+ errMessage);
				Message out=getMessageProcess().getFinalException(messageString,errMessage,e.getResponeType(),emailNotfMsg);
				exchange.getOut().setBody(out);

			}catch(Exception e){
				e.getMessage();
				e.printStackTrace();
				String	emailNotfMsg=null;
				System.out.println("Exception Catch Block"+e.getMessage());
				logger.info("Exception caught in Exception Catch block_Error Message");
				Message out=getMessageProcess().getFinalException(messageString,e.getMessage(),prop.getProperty("AcknowledgementError"),emailNotfMsg);
				exchange.getOut().setBody(out);
			}finally{
				prop1= null;
				prop = null;
			}
		}else if(prop1.containsKey("enable_ADT_Interface") == false || prop1.getProperty("enable_ADT_Interface").equalsIgnoreCase("N")
				||"".equals(prop1.getProperty("enable_ADT_Interface"))){
			logger.info("\n\n\n*******************************"
					+ "\n* PLEASE ENABLE ADT INTERFACE *\n"
					  + "*******************************");
			prop1= null;
			prop = null;
		}

		//Convert the String message to Hapi Message.

		//If message converts successfully, get the Message Header segment and identify the message type.

		//Based on message type, load the configurations. 

		//Parse the message, using reflection. For any missing segment, missing field as per the cardinality in the configuration we need to mark this message as 'AR'.
		//In this case throw an exception and in catch of that, generate a response with AR. Response segments for a message type will also come from the configuration.
		//Please note we might need to send an email notification also for a failed message, so please keep the code flexible such that you can that the notification component.

		//If message is parsed successfully, persist the message in the appropriate table/tables. And generate a successful response based on the configured segments.

		//Persist the message in Audit table for both the failed and passed messages.

		//All the above steps you can do using multiple class, multiple functions anyway you feel comfortable. At each step log the message with appropriate texts.

		//Please note I havn't yet specified what to do if string message fails to convert to Hapi Message. That is 'AE' response. I am still thinking how to handle that case.
		//Till then you can go ahead and start developing the other things.
	}


}
