package com.velos.integration.core.messaging;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.velos.epic.XmlParser;

public class OutboundClient implements MessageListener, ExceptionListener {

	private static Logger logger = Logger.getLogger("epicLogger");

	private OutboundSAXProcessor sax;
	Properties prop1 ;




	public OutboundSAXProcessor getSax() {
		return sax;
	}

	public void setSax(OutboundSAXProcessor sax) {
		this.sax = sax;
	}

	private boolean running = true;

	// Fill out the properties like these example strings
	private static String connectionFactoryJNDI;
	private static String topicJNDI;
	private static String userName;
	private static String password;
	private static String clientID;
	private static String subscriberName;
	private int reStartCount = 0;
	public String getConnectionFactoryJNDI() {
		return connectionFactoryJNDI;
	}

	public void setConnectionFactoryJNDI(String connectionFactoryJNDI) {
		this.connectionFactoryJNDI = connectionFactoryJNDI;
	}

	public String getTopicJNDI() {
		return topicJNDI;
	}

	public void setTopicJNDI(String topicJNDI) {
		this.topicJNDI = topicJNDI;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getSubscriberName() {
		return subscriberName;
	}

	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}

	public void onException(JMSException arg0) {
		logger.error("Study Jboss Error ",arg0);
		logger.error("eReserach application server is down please start the server OR it may be restarted.So  please stop and start the Interface server");
		//arg0.printStackTrace();
		
		
		running = false;
		
		Properties prop = new Properties();
		try {

			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("velos-messaging.properties"));
		} catch (IOException e) {
			logger.info("error : " + e.getMessage());
			e.printStackTrace();
		}
			
		String serverUrl = prop.getProperty("serverUrl");
		
		boolean status = checkServerStatus(serverUrl);
		
		while(!status){
			status = checkServerStatus(serverUrl);
		}
		
		if(status){
			logger.info("Reconnecting  to the Study Topic");
			reStartCount = reStartCount+1;
			logger.info("Study Topic reconnect count = "+reStartCount);
			reconnectToTopic();
		}
		
	}
	
	private boolean checkServerStatus(String serverUrl) {

		boolean status = false;
		try{
			//String serverUrl = "http://192.168.192.179:8080";
			//String serverUrl = "http://192.168.192.179:8080/jmx-console";
			URL url = new URL(serverUrl);
			HttpURLConnection connection = null;
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			int code = connection.getResponseCode();
			
			if (code>=200 && code <= 209){

				logger.info("eResearch app server is started");
				status = true;

		    }else {
				logger.info("eResearch app server is not started");
		    	status = false;
		    }
			
		}catch(Exception e){
			status = false;
			return status;
		}
		return status;
	}
	private void reconnectToTopic() {

		boolean reconnect = true;
		
		running = true;
		
		while(reconnect){
			try{
				reconnect = false;
				this.run();
			}catch(Exception e){
				reconnect = true;
			}
		}
	}

	public void onMessage(Message msg) {
		logger.info("*******************************************");

		logger.info("Received Message : " + msg.toString());
		logger.info("got New message");
		if (msg instanceof TextMessage) {

			TextMessage xml = (TextMessage) msg;
			try {
				String xmlString = xml.getText();
				logger.info("xmlString : " + xmlString);
				this.getSax().processBySAXParser(xmlString);
			} catch (JMSException e) {
				logger.error("JMS Study Topic Error :"+e);
				e.printStackTrace();
			}
		}
	}

	public void startListening() {
		logger.info("startListening for Study Topic");


		try {
			


				ConnectionFactory factory = getConnectionFactory(connectionFactoryJNDI);

				Connection conn = factory.createConnection(userName, password);
				conn.setClientID(clientID);
				Session session = conn.createSession(false,
						Session.AUTO_ACKNOWLEDGE);
				Topic topic = getTopic(topicJNDI);
				TopicSubscriber sub = session.createDurableSubscriber(topic,
						subscriberName);
				sub.setMessageListener(this);
				conn.setExceptionListener(this);
				conn.start();
				while (running) { // listen messages
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						logger.error("JMS Study Topic InterruptedException :"+e);
						e.printStackTrace();
					}
				}
				conn.close();
			
		} catch (JMSException e) {
			logger.error("JMS Study Topic Exception ",e);
			logger.error("stop and start the Interface server properly");
			//e.printStackTrace();
		}
	}

	public Object lookup(String JNDIName) {
		Hashtable<String, String> env = new Hashtable<String, String>();
		InitialContext context = null;
		Object obj = null;
		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, ServiceProperties
					.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_INITIAL));
			env.put(Context.PROVIDER_URL, ServiceProperties
					.getProperty(ServiceProperties.JNDI_NAMING_PROVIDER_URL));
			env.put(Context.URL_PKG_PREFIXES,
					ServiceProperties
					.getProperty(ServiceProperties.JNDI_NAMING_FACTORY_URL_PKGS));
			context = new InitialContext(env);
			obj = context.lookup(JNDIName);
		} catch (NamingException e) {
			logger.error("JMS Study Topic NamingException",e);
			logger.error("eReserach application server is down please start the server and then stop and start the Interface server");
			//e.printStackTrace();
		}
		return obj;
	}

	public ConnectionFactory getConnectionFactory(String connectionFactoryJNDI) {

		ConnectionFactory factory = (ConnectionFactory) lookup(connectionFactoryJNDI);
		return factory;
	}

	public Topic getTopic(String topicJNDI) {
		Topic topic = (Topic) lookup(topicJNDI);
		return topic;
	}

	public void close() {
		this.running = false;
	}

	public void run() {
		logger.info("Inside run method : ");
		Properties prop = new Properties();

		prop1 = new Properties();
		try {
			prop1.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			logger.error("Config.Properties file doesn't Exist");
		}				

		logger.info("OutboundClient");
		if(prop1.containsKey("enable_Epic_Interface") == true && prop1.getProperty("enable_Epic_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop1.getProperty("enable_Epic_Interface"))){
			try {


				prop.load(this.getClass().getClassLoader()
						.getResourceAsStream("velos-messaging.properties"));
				connectionFactoryJNDI = prop
						.getProperty("study.topic.connectionfactory.jndi");
				topicJNDI = prop.getProperty("study.topic.jndi");
				userName = prop.getProperty("study.topic.username");
				password = prop.getProperty("study.topic.password");
				clientID = prop.getProperty("study.topic.client");
				subscriberName = prop.getProperty("study.topic.subscriber");

				logger.info("connectionFactoryJNDI : " + connectionFactoryJNDI);
				logger.info("topicJNDI : " + topicJNDI);
				logger.info("userName : " + userName);
				logger.info("password : " + password);
				logger.info("clientID : " + clientID);
				logger.info("subscriberName : " + subscriberName);	
			} catch (IOException e) {
				logger.info("error : " + e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.setTopicJNDI(ServiceProperties
					.getProperty(ServiceProperties.STUDY_TOPIC_JNDI));
			this.setConnectionFactoryJNDI(ServiceProperties
					.getProperty(ServiceProperties.STUDY_TOPIC_CONNECTION_FACTORY_JNDI));
			this.setUserName(ServiceProperties
					.getProperty(ServiceProperties.STUDY_TOPIC_USERNAME));
			this.setPassword(ServiceProperties
					.getProperty(ServiceProperties.STUDY_TOPIC_PASSWORD));
			this.startListening();
		}
	}
}
