package com.velos.integration.core.messaging;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PatientOutboundClientThread implements Runnable {

	private static Logger logger = Logger.getLogger("epicLogger");

	@Override
	public void run() {
		logger.info("Inside run method : ");
	Properties prop = new Properties();
		
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e1) {
			logger.error("***config.properties file missmatched***");		}
		if(prop.containsKey("enable_Epic_Interface") == true && prop.getProperty("enable_Epic_Interface").equalsIgnoreCase("y")
				&& !"".equals(prop.getProperty("enable_Epic_Interface"))){
	

		ClassPathXmlApplicationContext context= new ClassPathXmlApplicationContext(
				"ctms-config.xml");
	
		PatientOutboundClient patientClient = (PatientOutboundClient) context
				.getBean("patientOutboundClient");
		logger.info("PatientOutboundClient");
		try {
			patientClient.run();
		} catch (Exception e) {
			logger.info("error : " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			context.close();
		}
		}
	}
}
