package com.velos.integration.core.messaging;

public interface IdentifierConstants {
	
	public static String SIMPLE_IDENTIFIER = "simpleIdentifier";
	public static String STUDY_IDENTIFIER ="studyIdentifier"; 
	public static String STUDY_STATUS_IDENTIFIER = "studyStatusIdentifier"; 
	public static String ORGANIZATION_IDENTIFIER = "organizationIdentifier"; 
	public static String USER_IDENTIFIER = "userIdentifier"; 

}
