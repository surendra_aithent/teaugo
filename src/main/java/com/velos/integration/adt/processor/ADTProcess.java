package com.velos.integration.adt.processor;
//Last modified by surendra at 19May2015

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.camel.CamelContext;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import scala.annotation.meta.getter;
import ca.uhn.hl7v2.model.Message;

import com.velos.integration.adt.dao.ADTDao;
import com.velos.integration.hl7.dao.MessageDao;
import com.velos.integration.hl7.notifications.EmailNotification;
import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.integration.hl7.processor.MessageProcess;
import com.velos.integration.hl7.util.StringToXMLGregorianCalendar;
import com.velos.services.Code;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientSearch;
import com.velos.services.PatientStudy;

public class ADTProcess extends MessageProcess {

	private static Logger logger = Logger.getLogger(ADTProcess.class);

	private ADTDao adtDao;
	private ADTPostProcessorHelper aDTPostProcessorHelper;
	private StudyContacts studyContacts;

	Properties prop1 = new Properties();
	Properties prop2 = new Properties();

	private MessageDao messagedao = new MessageDao();
	private EmailNotification emailnotification= getEmailNotification();
	Properties prop = new Properties();

	public ADTProcess(MessageProcess messageProcess) throws HL7CustomException {
		super(messageProcess);
		adtDao = new ADTDao(messageProcess.getMessageDao());
		aDTPostProcessorHelper = new ADTPostProcessorHelper();

		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("adt.properties"));
			prop1.load(this.getClass().getClassLoader()
					.getResourceAsStream("ackcodes.properties"));
			prop2.load(this.getClass().getClassLoader()
					.getResourceAsStream("email.properties"));

			System.out.println("ADT  PRocess");
		} catch (Exception e) {
			throw new HL7CustomException(e.getMessage(),
					prop1.getProperty("AcknowledgementReject"));
		}
	}

	public ADTDao getAdtDao() {
		return adtDao;
	}

	public void setAdtDao(ADTDao adtDao) {
		this.adtDao = adtDao;
	}

	public boolean preProcessMerge(String tableName,
			Map<String, String> resultMap) throws HL7CustomException {

		try {

			boolean flag=checkNewMrn(tableName,resultMap );
			if(flag == true){

				logger.info("**************Update pre Mrg Record Starts****************");
				StringBuffer sqlMerge = new StringBuffer("select");
				String delim = "";
				StringBuffer whrSqlMerge = new StringBuffer();

				String mrnField = "";

				int i = 0;
				for (Entry<Object, Object> e : prop.entrySet()) {

					if ("select".equals(e.getValue().toString())
							|| e.getKey().toString().contains("mrg.")) {
						sqlMerge.append(delim);

						sqlMerge.append(e.getKey().toString().substring(4));

						delim = ", ";
						i++;
					}
				}

				sqlMerge.append(" "+prop.get("merg.mrnFieldName")+" from " + tableName + " where ");

				sqlMerge.append(" pk_" + tableName + " = (select max(pk_"
						+ tableName + ") from " + tableName + " where "
						+ prop.getProperty("merg.mrnFieldName") + " = '"
						+ resultMap.get(prop.getProperty("merg.mergeField")) + "')");

				logger.info("sqlMergeQuery  " + sqlMerge);
				HashMap<String, String> record = adtDao.getRecord(sqlMerge
						.toString());

				int count = 0;


				if (record != null && record.size() >= 1) {
					// pick the latest record with priorMRN
					// if record != null then check record Map values with resultMap
					// values
					// if it is equal then update the record in vgdb

					for (Entry<Object, Object> e : prop.entrySet()) {
						if ("select".equals(e.getValue().toString())
								|| e.getKey().toString().contains("mrg.")) {

							String result = resultMap.get(e.getKey().toString()
									.substring(4));
							String recordVal = record.get(e.getKey().toString()
									.substring(4));

							if (result != null && recordVal != null
									&& result.equalsIgnoreCase(recordVal)) {

								count = count + 1;
							}

						}
					}
					logger.info("Count--->"+count +"  "+ i + " Size---> " +record.size());
					// if all Patient Demographics matches then update the record
					// with New MRN
					if (count == i  && record != null) {
						logger.info("**************Update Mrg Record query****************");

						logger.info("Update Merge Message");
						//call post processing function
						return true;

						// postProcess(tableName, resultMap);

					}

					else {

						// Patient demographics are not matches, so send error
						// notification
						// stating with
						// mrn, firstname, lastname, dob, race, gender details from
						// both the
						// current and previous record. are not matches
						// emailNotification.sendMail("Patient Demographics doesnot match for prior MRN");
						logger.info("PatientDemographics  doesnot Match for priro MRN in VGDB");
						throw new HL7CustomException(
								prop1.getProperty("merg_message_nodemg_VGDB"),
								prop1.getProperty("AcknowledgementReject"));

					}
				}
				// if record Map == null means there is no Patient Demographics with
				// PriorMRN
				if (record == null) {

					// check all vgdb records with current Patient Demographics
					// if it matches send Notification Demographics matches buts MRN
					// doesn't match
					logger.info("*******************Merge Patient Demographics in VGDB*************");

					logger.info("****************prop.getProperty(mrg_patientdemgcomparsonReq).toString()*******************" + prop.getProperty("mrg_patientdemgcomparsonReq").toString());
					logger.info("****************prop.containsKey(mergeEvents)*******************" + prop.containsKey("mergeEvents"));
					logger.info("****************resultMap.containsKey(eventid)*******************" + resultMap.containsKey("eventid"));
					logger.info("****************(prop.getProperty(mergeEvents).toString().contains(resultMap.get(eventid))*******************" + prop.getProperty("mergeEvents").toString().contains(resultMap.get("eventid")));


					if("Y".equals(prop.getProperty("mrg_patientdemgcomparsonReq").toString().trim())
							&& prop.containsKey("mergeEvents")
							&& resultMap.containsKey("eventid")
							&& prop.getProperty("mergeEvents").toString()
							.contains(resultMap.get("eventid"))) {


						sqlMerge = new StringBuffer("select ");
						delim = "";
						sqlMerge.append(prop.getProperty("merg.mrnFieldName"));
						sqlMerge.append(" from " + tableName + " where ");

						for (Entry<Object, Object> e : prop.entrySet()) {
							if ("select".equals(e.getValue().toString())
									|| e.getKey().toString().contains("mrg.")) {

								sqlMerge.append(delim);
								sqlMerge.append(e.getKey().toString().substring(4)
										+ " = '"
										+ resultMap.get(e.getKey().toString()
												.substring(4)) + "'");
								delim = " AND ";
							}

						}

						sqlMerge.append(" order by pk_" + tableName + " desc ");

						logger.info("Merge Select Query with patient demographics  "
								+ sqlMerge);
						record = adtDao.getRecord(sqlMerge.toString());

					}

					if (record != null) {
						// Send you email notification here stating that no previous
						// record
						// exist for this merge message.
						// With notification you need to send the details also, that
						// for
						// which ADT merge message this notification is generated.
						// You
						// need
						// to create template for that
						// It will include things like mrn, firstname, lastname,
						// dob,
						// race,
						// gender. Please take Ram's help on how the notification
						// should
						// look like.
						// emailNotification.sendMail("Patient Demographics are matches but Prior MRN doesn't match");
						logger.info("Patient Demographics are matches but Prior MRN doesn't match");
						throw new HL7CustomException(
								prop1.getProperty("merg_message_nomrn_CTMS"),
								prop1.getProperty("AcknowledgementReject"));

					} else {
						// emailNotification.sendMail("Neither Patient Demographics nor MRN matches with any previous record");
						logger.info("Neither Patient Demographics nor MRN does not matches with any previous record");
						throw new HL7CustomException(
								prop1.getProperty("merg_message_nomrn"),
								prop1.getProperty("AcknowledgementReject"));

						// Both Demographics and MRN mismatch. Send notification
						// accordingly.

					}


				}

			}

		} catch (HL7CustomException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		return true;
	}

	public boolean preProcessUpdate(String tableName,
			Map<String, String> resultMap) throws Exception {
		try {
			StringBuffer sqlUpdate = new StringBuffer(
					"select count(*) as count");

			sqlUpdate.append(" from " + tableName + " where "
					+ prop.getProperty("upd.mrnFieldName") + " = "
					+ resultMap.get(prop.getProperty("upd.mrnFieldName")));

			HashMap<String, String> record = adtDao.getRecord(sqlUpdate
					.toString());

			if (record.containsKey("count")
					&& (Integer.parseInt(record.get("count")) == 0)) {
				// Send Error notification as no previous records for update
				// event.
				return false;
			}
		} catch (HL7CustomException e) {
			throw e;
		}

		return true;
	}

	public boolean preProcess(String tableName, Map<String, String> resultMap,
			Message input) throws Exception {
		// Apply your pre process logic here. This will include merging patients
		// in VGDB.
		logger.info("****************Message Pre Process*******************");
		logger.info("*******************Pre Processing*************");
		logger.info("Event Id"+resultMap.get("eventid"));
		logger.info("MergeEvents --->"+prop.containsKey("mergeEvents")
				+"   "+resultMap.containsKey("eventid"));

/*		logger.info("contain in result map--->"+ prop.getProperty("mergeEvents").toString()
				.contains(resultMap.get("eventid")));*/
		try {

			if ("Y".equals(prop.getProperty("preMergeReq").toString().trim())) {
				if (prop.containsKey("mergeEvents")
						&& resultMap.containsKey("eventid")
						&& (prop.getProperty("mergeEvents").toString()
								.contains(resultMap.get("eventid")))) {
					logger.info("*******************Pre Processing Starts************");
					return preProcessMerge(tableName, resultMap);
				}
			}
			if ("Y".equals(prop.getProperty("preUpdateReq").toString())) {
				if (prop.containsKey("updateEvents")
						&& resultMap.containsKey("eventid")
						&& (prop.getProperty("updateEvents").toString()
								.contains(resultMap.get("eventid")))) {
					return preProcessUpdate(tableName, resultMap);
				}
			}
		} catch (HL7CustomException e) {
			throw e;
		}
		return true;
	}

	@SuppressWarnings("unused")
	public boolean postProcess(String tablename, Map<String, String> resultMap)
			throws Exception {
		// Apply you post process logic here. This will include updating/merging
		// patients in eResearch
		// int count=0;
		logger.info("****************ADT Post Process*******************");
		logger.info("****************ADT Post Process*******************");
		if ("Y".equals(prop.getProperty("postMergeReq").toString())) {
			if (prop.containsKey("mergeEvents")
					&& resultMap.containsKey("eventid")
					&& (prop.getProperty("mergeEvents").toString()
							.contains(resultMap.get("eventid")))) {
				logger.info("****PostProcess***");
				List<PatientDataBean> patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "mrg_eResearch",
								2);

				logger.info("Patient data list--------->"+patientDataList.size());

				if (patientDataList != null || patientDataList.size() != 0) {

					compareEresValues(patientDataList,tablename,
							resultMap,3);

				} 
				if(patientDataList == null || patientDataList.size() == 0) {

					List<PatientDataBean> patientDmgDataList = aDTPostProcessorHelper
							.searchPatientHelper(resultMap, prop, "mrg_eResearch",
									3);

					if(patientDmgDataList != null || patientDmgDataList.size() != 0){


						Map<String,String> res=ComparePatientDemographics(patientDmgDataList,resultMap);

						int count=Integer.parseInt(res.get("count"));
						//Number of Patient Demographic fields to compare in CTMS DB, config in adt.properties file
						int i=Integer.parseInt(prop.getProperty("numberoffieldstocompare"));
						logger.info("Count------>"+count);
						//if("Y".equals(prop.getProperty("mrg_patientdemgcomparsonReq").toString().trim()))
						if(count == i && "Y".equals(prop.getProperty("mrg_patientdemgcomparsonReq").toString().trim()) ){

							throw new HL7CustomException(
									prop1.getProperty("merg_message_nomrn_CTMS"),
									prop1.getProperty("AcknowledgementReject"));
						}else{
							// emailNotification.sendMail("Neither Patient Demographics nor MRN matches with any previous record");
							logger.info("Neither Patient Demographics nor MRN does not matches with any previous record");
							throw new HL7CustomException(
									prop1.getProperty("merg_message_nomrn"),
									prop1.getProperty("AcknowledgementReject"));
							// Both Demographics and MRN mismatch. Send notification
						}
					}
				}
			}
		}
		logger.info("****************prop.getProperty(postUpdateReq).toString()*******************" + prop.getProperty("postUpdateReq").toString());
		logger.info("****************prop.containsKey(updateEvents)*******************" + prop.containsKey("updateEvents"));
		logger.info("****************resultMap.containsKey(eventid)*******************" + resultMap.containsKey("eventid"));
		logger.info("****************(prop.getProperty(updateEvents).toString().contains(resultMap.get(eventid))*******************" + prop.getProperty("updateEvents").toString().contains(resultMap.get("eventid")));
		if ("Y".equals(prop.getProperty("postUpdateReq").toString())) {
			if (prop.containsKey("updateEvents")
					&& resultMap.containsKey("eventid")
					&& (prop.getProperty("updateEvents").toString()
							.contains(resultMap.get("eventid")))) {
				List<PatientDataBean> patientDataList = null;
				try{
					patientDataList = aDTPostProcessorHelper
							.searchPatientHelper(resultMap, prop, "upd_eResearch",
									2);
				}catch(HL7CustomException e){
				
					throw e;
				}
				logger.info("****************Update patientDataList*******************" + patientDataList.size());

				logger.info("****************patientDataList*******************" + patientDataList.size());
				if (patientDataList != null) {
					// If Configuration says throw error and notification for
					// multiple records check if multiple records and take
					// appropriate actions.
					// If no errors call update patient facility with new
					// demographics details
					if ("Y".equals(prop.get("multipleUpdates").toString()) || ("N".equals(prop.get("multipleUpdates").toString()) && patientDataList.size() == 1)) {

						aDTPostProcessorHelper.updatePatientHelper(resultMap,
								prop, "upd_eResearch", patientDataList);


					} else if ("N".equals(prop.get("multipleUpdates").toString()) && patientDataList.size() > 1) {
						throw new HL7CustomException(
								prop1.getProperty("merg_message_multiplerec_CTMS"),
								prop1.getProperty("AcknowledgementReject"));
					}
				}

			}
		}
		return true;
	}

	public Map<String,String> ComparePatientDemographics(
			List<PatientDataBean> patientDataList, Map<String, String> resultMap) throws HL7CustomException  {
		Map<String,String> patDemglistcount= new HashMap<String, String>();

		try{
			int count=0;
			String resultVal = null;
			StringBuffer eresDemg = new StringBuffer("");
			if (  prop.containsKey("cmpr_eResearch."+"PatFirstName")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatFirstName"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatFirstName();
				if(resultVal != null){
					logger.info("Result Value----"+resultVal);

					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatFirstName").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient FirstName :: "+resultVal);
						eresDemg.append("\n");

					}
				}}


			if (  prop.containsKey("cmpr_eResearch."+"PatLastName")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatLastName"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatLastName();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatLastName").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient LastName :: "+resultVal);
						eresDemg.append("\n");
					}
				}
			}

			if (  prop.containsKey("cmpr_eResearch."+"Gender")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"Gender"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getGender().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"Gender").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Gender :: "+resultVal);
						eresDemg.append("\n");
					}
				}}

			if (  prop.containsKey("cmpr_eResearch."+"Race")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"Race"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getRace().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"Race").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Race :: "+resultVal);
						eresDemg.append("\n");
					}
				}
			}
			if (  prop.containsKey("cmpr_eResearch."+"PatDateofBirth")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatDateofBirth"))) {
				for(PatientDataBean pdb : patientDataList){
					resultVal=pdb.getPatDateofBirth();}
				if(resultVal != null){
					logger.info("Result Value----"+resultVal);
					logger.info("DOB--------->"+resultMap.get(prop.getProperty("cmpr_eResearch." +"PatDateofBirth").toString().trim()));
					if(resultVal.equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatDateofBirth").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient DateofBirth :: "+resultVal);
						eresDemg.append("\n");
					}}
			}
			if (  prop.containsKey("cmpr_eResearch."+"PatientFacilityId")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatientFacilityId"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatientFacilityId();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatientFacilityId").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Facility Id :: "+resultVal);
						eresDemg.append("\n");
					}
				}}
			if (  prop.containsKey("cmpr_eResearch."+"PatientIdentifier")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatientIdentifier"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatientIdentifier().getPatientId();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatientIdentifier").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Identifier ::  "+resultVal);
						eresDemg.append("\n");
					}
				}
			}


			if (  prop.containsKey("cmpr_eResearch."+"PatSurvivalStat")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatSurvivalStat"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatSurvivalStat().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatSurvivalStat").toString().trim()))){
						count=count+1;

					}
				}
			}





			patDemglistcount.put("patientdemg",eresDemg+"");
			patDemglistcount.put("count", count+"");

		}catch(Exception e){
			throw new HL7CustomException(
					e.getMessage()+" in ComparePatientDemographics method",
					prop1.getProperty("AcknowledgementError"));
		}

		return patDemglistcount;


	}

	@SuppressWarnings("null")
	private boolean compareEresValues(List<PatientDataBean> patientDataList,String tableName,
			Map<String, String> resultMap, int searchType)
					throws HL7CustomException, Exception {

		StringBuffer eresDemg =new StringBuffer("");
		StringBuffer reslutMapDemg =new StringBuffer("");
		String rsltMapMrn;
		String msgId;

		rsltMapMrn = resultMap
				.get(prop.getProperty("merg.mergeField"));

		msgId=resultMap.get(prop.getProperty("msgcontrolid_columnName"));
		boolean flag=true;

		reslutMapDemg = getMrgVgdbPtDmg(resultMap,flag);


		// comparing eResearch patient demographics with resultMap demographics
		// if patdemographics matches send error response with eres patdemogrphics
		// and Incoming Msg Demographics
		// if PatDemographics doesn,t matches return false

		if (searchType == 3) {
			StringBuffer patDemg=new StringBuffer("");
			String resultVal = null;
			int count=0;
			logger.info("PatientDataList size compareeResMethod------>"+patientDataList.size());

			if (  prop.containsKey("cmpr_eResearch."+"PatFirstName")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatFirstName"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatFirstName();
				if(resultVal != null){

					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatFirstName").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient FirstName :: "+resultVal);
						eresDemg.append("\n");

					}
				}}


			if (  prop.containsKey("cmpr_eResearch."+"PatLastName")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatLastName"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatLastName();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatLastName").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient LastName :: "+resultVal);
						eresDemg.append("\n");
					}
				}
			}

			if (  prop.containsKey("cmpr_eResearch."+"Gender")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"Gender"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getGender().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"Gender").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Gender :: "+resultVal);
						eresDemg.append("\n");
					}
				}}

			if (  prop.containsKey("cmpr_eResearch."+"Race")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"Race"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getRace().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"Race").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Race :: "+resultVal);
						eresDemg.append("\n");
					}
				}
			}
			if (  prop.containsKey("cmpr_eResearch."+"PatDateofBirth")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatDateofBirth"))) {
				for(PatientDataBean pdb : patientDataList){
					resultVal=pdb.getPatDateofBirth();}
				if(resultVal != null){
					logger.info("Result Value----"+resultVal);
					logger.info("DOB--------->"+resultMap.get(prop.getProperty("cmpr_eResearch." +"PatDateofBirth").toString().trim()));
					if(resultVal.equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatDateofBirth").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient DateofBirth :: "+resultVal);
						eresDemg.append("\n");
					}}
			}
			if (  prop.containsKey("cmpr_eResearch."+"PatientFacilityId")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatientFacilityId"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatientFacilityId();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatientFacilityId").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Facility Id :: "+resultVal);
						eresDemg.append("\n");
					}
				}}
			if (  prop.containsKey("cmpr_eResearch."+"PatientIdentifier")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatientIdentifier"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatientIdentifier().getPatientId();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatientIdentifier").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Identifier ::  "+resultVal);
						eresDemg.append("\n");
					}
				}
			}


			if (  prop.containsKey("cmpr_eResearch."+"PatSurvivalStat")
					&& !"".equals(prop.getProperty("cmpr_eResearch." +"PatSurvivalStat"))) {
				for(PatientDataBean pdb : patientDataList)
					resultVal=pdb.getPatSurvivalStat().getCode();
				if(resultVal != null){
					if(resultVal.toString().equalsIgnoreCase(resultMap.get(prop.getProperty("cmpr_eResearch." +"PatSurvivalStat").toString().trim()))){
						count=count+1;
						eresDemg.append("Patient Survival Status :: "+resultVal);
						eresDemg.append("\n");
					}
				}
			}

			logger.info("PatientDataList2 size compareeResMethod------>"+patientDataList.size());
			eresDemg.append(prop2.getProperty("mail.msgbody2").toString().trim());

			for (PatientDataBean pdb : patientDataList) {
				// Comparing eResearchMrn with resultMap Mrn 
				//if it matches sending error with demographics
				logger.info("Merge process CTMS PatientDataListLength"+patientDataList.size());
				logger.info("Merge PriorMrn in resultMap--->"+rsltMapMrn+" -Mrn in CTMS-->  "+pdb.getPatientFacilityId());
				//comparing CTMS MRN with Incoming Msg PriorMrn 
				if (pdb.getPatientFacilityId().trim().equals(rsltMapMrn)) {
					eresDemg.append("\n\nNew Patient MRN"+"\t : "+ isEmpty(pdb.getPatientFacilityId()));
					eresDemg.append("\n"); eresDemg.append("\n");
					eresDemg.append("Patient Name"+"\t : "+isEmpty(pdb.getPatLastName())+", "+isEmpty(pdb.getPatFirstName()));	
					eresDemg.append("\n");
					eresDemg.append("DOB"+"\t\t : "+isEmpty(pdb.getPatDateofBirth()));
					eresDemg.append("\n");
					eresDemg.append("Race"+"\t\t : "+isEmpty(pdb.getRace().getCode()));
					eresDemg.append("\n");
					eresDemg.append("Gender"+"\t\t : "+isEmpty(pdb.getGender().getCode()));
					eresDemg.append("\n");
					patDemg.append(eresDemg+"\n");

				}}

			logger.info("compareeRes");

			//Number of Patient Demographic fields to compare in CTMS DB, config in adt.properties file
			int i=Integer.parseInt(prop.getProperty("numberoffieldstocompare"));
			logger.info(" VGDB demographics count-->"+ count +"properties file count--> "+ i);
			//Demographics are equal merge the records in CTMS
			if(count == i ){


				if ("Y".equals(prop.get("multipleMergUpdates").toString()) || ("N".equals(prop.get("multipleMergUpdates").toString()) && patientDataList.size() == 1)) {
					logger.info("****Merge PostProcess-2***");

					logger.info("Merge Update Starts");

					//PatientDemographic update in CTMS
					aDTPostProcessorHelper.updatePatientHelper(resultMap,
							prop, "upd_eResearch", patientDataList);

					logger.info("Merge Demographic Starts");

					//MRN Update in CTMS
					aDTPostProcessorHelper.updatePatientFacilityHelper(
							resultMap, prop, "mrg_eResearch",
							patientDataList);

					mergUpdateVGDB(tableName,resultMap);


					logger.info("Demographics----------->"+reslutMapDemg+" \n  "+patDemg);

					logger.info("Merge Success" + prop2.getProperty("mail.msgbody2").toString().trim()+"   \n "+ prop2.getProperty("mail.msgbody2").toString().trim());
					logger.info("********************Merge Updated Process End Successfully*******************\n"+reslutMapDemg+"\n\n"+patDemg);

					logger.info("Merge Success" + prop2.getProperty("mail.msgbody2").toString().trim());
					/*throw new HL7CustomException("Merge Update Successfully in CTMS",
							"MRN Matches\n"+prop2.getProperty("mail.msgbody1").toString().trim()+"\n"
									+ reslutMapDemg
									+ "\n"+prop2.getProperty("mail.msgbod2").toString().trim()+"\n" + patDemg,
							"AA");

					 */			

					if("Y".equalsIgnoreCase(prop.getProperty("emailNotf_Require").toString().trim())){

						// searchType 1 means search with MRN, 2 means PRIORMRN 
						String studyCoordList =	getStudyCoordinators(resultMap,prop,1);
						 
						if(studyCoordList == null || "".equals(studyCoordList)){
							logger.info("Patient Not enrolled to any study / email not configured");
						}
						System.out.println("Study contacts =====>"+ studyCoordList);
						emailnotification.sendMail(reslutMapDemg.toString(),patDemg.toString(), messagedao.getDate(), rsltMapMrn, msgId,studyCoordList);
					}
					//Merge Success
				} else if ("N".equals(prop.get("multipleMergUpdates").toString()) && patientDataList.size() > 1) {
					throw new HL7CustomException(
							prop1.getProperty("merg_message_multiplerec_CTMS"),
							prop1.getProperty("AcknowledgementReject"));
				}



			}
			else if(count != i){

				logger.info("PatientDemographics  doesnot Match for priro MRN in VGDB");
				throw new HL7CustomException(
						prop1.getProperty("merg_message_nodemg_CTMS"),
						prop1.getProperty("AcknowledgementReject"));
			}
		}

		return false;

	}


	//*************** STUDY COORDINATORS Starts ***********************
	public String getStudyCoordinators(Map<String, String> resultMap, Properties prop,int searchType) throws OperationException_Exception, HL7CustomException{


		String studyCoordinators = "";
		if(prop.containsKey("studyCont_search__eResearch_MRN.patientFacilityId")&& 
				!"".equals(prop.getProperty("studyCont_search__eResearch_MRN.patientFacilityId"))){
			// searchType 1 means search with MRN, 2 means PRIORMRN 

			logger.info("****************Study Contacts Process Starts*****************");

			studyContacts = new StudyContacts();
			studyCoordinators	= studyContacts.getStudyContacts(resultMap, prop,searchType);
			
			if(studyCoordinators == null || studyCoordinators.equals("")){
				
				return null;
			}

			logger.info("StudyContacts ==========>"+studyCoordinators);
			System.out.println("StudyContacts ==========>"+studyCoordinators);
			logger.info("****************Study Contacts Process Ends*****************");

		}else{
			logger.info("No Study coordinator email notification functionality");
		}


		return studyCoordinators;
	}

	//******************* STUDY COORDINATORS Ends ***********************




	// getting incoming merge message values using properties file values
	//to display in email message
	public StringBuffer getMrgVgdbPtDmg(Map<String, String> resultMap,boolean flag) {
		logger.info("*** Getting VGDB Patient Demographics******");
		String rsltMapDmg;
		StringBuffer reslutMapDemg=new StringBuffer("");
		reslutMapDemg.append(prop2.getProperty("mail.msgbody1").toString().trim()+"\n\n");
		if(flag == false){
			rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.priormrn"));
			reslutMapDemg.append("Prior Patient MRN"+"\t : "+rsltMapDmg);
			reslutMapDemg.append("\n"); reslutMapDemg.append("\n");
		}

		rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.mrn"));
		reslutMapDemg.append("New Patient MRN"+"\t : "+rsltMapDmg);
		reslutMapDemg.append("\n"); 
		reslutMapDemg.append("\n");
		rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.firstname"));
		reslutMapDemg.append("Patient Name"+"\t : "+isEmpty(resultMap.get(prop.getProperty("merge_vgdb.lastname")))
				+", "+rsltMapDmg+"  "+isEmpty(resultMap.get(prop.getProperty("merge_vgdb.middlename"))));

		reslutMapDemg.append("\n");
		rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.dob"));
		reslutMapDemg.append("DOB"+"\t\t : "+rsltMapDmg);
		reslutMapDemg.append("\n");
		rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.race"));
		reslutMapDemg.append("Race"+"\t\t : "+rsltMapDmg);
		reslutMapDemg.append("\n");
		rsltMapDmg = resultMap.get(prop.getProperty("merge_vgdb.gender"));
		reslutMapDemg.append("Gender"+"\t\t : "+rsltMapDmg);
		reslutMapDemg.append("\n");


		return reslutMapDemg;
	}



	public StringBuffer getCTMSDemg(List<PatientDataBean> patientDataList,boolean flag){
		logger.info("*** Getting CTMS Patient Demographics******");
		StringBuffer eresDemg =new StringBuffer("");
		String field = null ;

		logger.info("patient Data List "+ patientDataList);

		if(flag == false){
			field = "Prior Patient MRN \t";
		}else{
			field ="New Patient MRN \t";
		}

		for (PatientDataBean pdb : patientDataList) {
			eresDemg.append("\n\n"+field+" : "+ isEmpty(pdb.getPatientFacilityId()));
			eresDemg.append("\n"); eresDemg.append("\n");
			eresDemg.append("Patient Name"+"\t : "+isEmpty(pdb.getPatLastName())+", "+isEmpty(pdb.getPatFirstName()));

			eresDemg.append("\n");
			eresDemg.append("DOB"+"\t\t : "+isEmpty(pdb.getPatDateofBirth()));
			eresDemg.append("\n");
			/*
			eresDemg.append("Race"+"\t\t : "+pdb.getRace().getCode());
			eresDemg.append("\n");
			eresDemg.append("Gender"+"\t\t : "+isEmpty(pdb.getGender().getCode()));*/
			
			if(pdb.getRace()!=null){
				eresDemg.append("Race"+"\t\t : "+isEmpty(pdb.getRace().getCode()));
			}else{
				eresDemg.append("Race"+"\t\t : "+"");
			}
			
			eresDemg.append("\n");
			
			
			if(pdb.getGender()!=null){
			eresDemg.append("Gender"+"\t\t : "+isEmpty(pdb.getGender().getCode()));
			}else{
				eresDemg.append("Gender"+"\t\t : "+"");	
			}
			eresDemg.append("\n");

		}
		logger.info("************after getting CTMS demographics**********");
		return eresDemg;

	}

	//update PatientDemographics in VGDB if Merge message success
	public boolean mergUpdateVGDB(String tableName,Map<String, String> resultMap) throws HL7CustomException{
		String mrnField="";
		boolean flag=false;
		String delim = "";
		StringBuffer sqlMerge = new StringBuffer("select");
		int i=0;

		for (Entry<Object, Object> e : prop.entrySet()) {

			if ("select".equals(e.getValue().toString())
					|| e.getKey().toString().contains("mrg.")) {
				sqlMerge.append(delim);

				sqlMerge.append(e.getKey().toString().substring(4));

				delim = ", ";
				i++;
			}
		}


		try {

			mrnField = prop.getProperty("merg.mrnFieldName");
			sqlMerge = new StringBuffer();
			sqlMerge.append("update "
					+ tableName
					+ " set "
					+ mrnField
					+ " ='"
					+ resultMap.get(mrnField)
					+ "',"
					+ prop.getProperty("merg.lastmodifydate")
					+ " ='"
					+ messagedao.getDate()
					+ "',"
					+ prop.getProperty("merg.msgstatuscol")
					+ "='"
					+ prop.getProperty("merg.msgstatus")
					+ "' where "
					+ mrnField
					+ " ='"
					+ resultMap.get(prop
							.getProperty("merg.mergeField")) + "'");
			adtDao.updateRecord(sqlMerge.toString());
			flag=true;
			logger.info("\nMerge Updated query with New MRN --->" + sqlMerge);

		} catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage(),
					prop1.getProperty("AcknowledgementError"));
		}
		return flag;
	}

	/*

	//Checking New MRN and PriorMRN in CTMS
	public boolean checkNewMrn(String tableName,Map<String, String> resultMap ) throws HL7CustomException, IOException{
		logger.info("****Check new mrn***");
		List<PatientDataBean> patientDataList,patientDataList1=new ArrayList<PatientDataBean>();
		patientDataList=null;
		patientDataList1=null;

		StringBuffer allCtmsDemg=new StringBuffer("");
		StringBuffer ctmsDemg=new StringBuffer("");
		StringBuffer reslutMapDemg =new StringBuffer(""); 
		allCtmsDemg.append("\n"+prop2.getProperty("mail.msgbody2").toString().trim());
		boolean nflag=false,pflag=false;

		try {

			if (prop.containsKey("chk_newMRN_eResearch" + ".patientFacilityId")
					&& !"".equals(prop.getProperty("chk_newMRN_eResearch" + ".patientFacilityId")
							.toString().trim())) {
				logger.info("****Check new mrn***");
				patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "chk_newMRN_eResearch",
								2);

				if(patientDataList.size()>=1){
					reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					ctmsDemg=getCTMSDemg(patientDataList,true);
					allCtmsDemg.append(ctmsDemg);
					nflag=true;
				}


			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage()+"Exception occurred while search in eResearch,  Please contact System Administrator",
					prop1.getProperty("AcknowledgementError"));
		}	

		if(patientDataList.size()>=1){
				logger.info("********* New patient already in SYSTEM *******");
				StringBuffer reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
				 ctmsDemg=getCTMSDemg(patientDataList);
				logger.info("*******Sending Error Email notification *****");
				nflag=true;
				//emailnotification.sendMail(reslutMapDemg.toString(),ctmsDemg.toString());
				logger.info("*******after Sending Error Email notification *****");

				throw new HL7CustomException(prop1.getProperty("chk_newmrn_message"),
						prop1.getProperty("AcknowledgementReject"));

			}

		try {

			if (prop.containsKey("chk_newMRN_eResearch" + ".patientFacilityId")
					&& !"".equals(prop.getProperty("chk_newMRN_eResearch" + ".patientFacilityId")
							.toString().trim())) {
				logger.info("****Check prior mrn***");
				patientDataList1 = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "chk_priorMRN_eResearch",
								2);

				if(patientDataList1.size()>1){
					reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					ctmsDemg=getCTMSDemg(patientDataList1,false);
					allCtmsDemg.append(ctmsDemg);
					pflag=true;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage()+"Exception occurred while search in eResearch,  Please contact System Administrator",
					prop1.getProperty("AcknowledgementError"));
		}	
		if(patientDataList1.size()>1){
					StringBuffer reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					 ctmsDemg=getCTMSDemg(patientDataList1);
					//emailnotification.sendMail(reslutMapDemg.toString(),ctmsDemg.toString());
					pflag=true;
				throw new HL7CustomException(prop1.getProperty("merg_message_multiplerec_CTMS"),
						prop1.getProperty("AcknowledgementReject"));
			}

		if(pflag == true){
			// call Study Coordinators method 
			//
			// String studyCoordinator = getStudyCoordinators(resultMap, prop,1);
			//emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString(),studyCoordinator);
			throw new HL7CustomException(prop1.getProperty("merg_message_multiplerec_CTMS"),
					prop1.getProperty("AcknowledgementReject"));

		} else if(pflag == true && nflag == true){

			// String newMrn_studyCoordinator = getStudyCoordinators(resultMap, prop,1);
			// String priorMrn_studyCoordinator = getStudyCoordinators(resultMap, prop,2);
			// String  studyCoordintaorList = newMrn_studyCoordinator +","+priorMrn_studyCoordinator;
			//emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString(),studyCoordintaorList);
			throw new HL7CustomException(prop1.getProperty("chk_newmrn_message"),
					prop1.getProperty("AcknowledgementReject"));

		} else if(nflag == true){
			//emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString(),"");
			throw new HL7CustomException(prop1.getProperty("chk_newmrn_message"),
					prop1.getProperty("AcknowledgementReject"));
		}


		return true;
	}
	 */



	//Checking New MRN and PriorMRN in CTMS
	public boolean checkNewMrn(String tableName,Map<String, String> resultMap ) throws HL7CustomException, IOException, OperationException_Exception{
		logger.info("****Check new mrn***");
		List<PatientDataBean> patientDataList,patientDataList1=new ArrayList<PatientDataBean>();
		patientDataList=null;
		patientDataList1=null;

		StringBuffer allCtmsDemg=new StringBuffer("");
		StringBuffer ctmsDemg=new StringBuffer("");
		StringBuffer reslutMapDemg =new StringBuffer(""); 
		allCtmsDemg.append("\n"+prop2.getProperty("mail.msgbody2").toString().trim());
		boolean nflag=false,pflag=false;

		try {

			if (prop.containsKey("chk_newMRN_eResearch" + ".patientFacilityId")
					&& !"".equals(prop.getProperty("chk_newMRN_eResearch" + ".patientFacilityId")
							.toString().trim())) {
				logger.info("****Check new mrn***");
				patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "chk_newMRN_eResearch",
								2);

				if(patientDataList.size()>=1){
					reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					ctmsDemg=getCTMSDemg(patientDataList,true);
					allCtmsDemg.append(ctmsDemg);
					nflag=true;
				}


			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage()+"Exception occurred while search in eResearch,  Please contact System Administrator",
					prop1.getProperty("AcknowledgementError"));
		}	

		/*if(patientDataList.size()>=1){
				logger.info("********* New patient already in SYSTEM *******");
				StringBuffer reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
				 ctmsDemg=getCTMSDemg(patientDataList);
				logger.info("*******Sending Error Email notification *****");
				nflag=true;
				//emailnotification.sendMail(reslutMapDemg.toString(),ctmsDemg.toString());
				logger.info("*******after Sending Error Email notification *****");

				throw new HL7CustomException(prop1.getProperty("chk_newmrn_message"),
						prop1.getProperty("AcknowledgementReject"));

			}*/

		try {

			if (prop.containsKey("chk_newMRN_eResearch" + ".patientFacilityId")
					&& !"".equals(prop.getProperty("chk_newMRN_eResearch" + ".patientFacilityId")
							.toString().trim())) {
				logger.info("****Check prior mrn***");
				patientDataList1 = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "chk_priorMRN_eResearch",
								2);

				if(patientDataList1.size()>1){
					reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					ctmsDemg=getCTMSDemg(patientDataList1,false);
					allCtmsDemg.append(ctmsDemg);
					pflag=true;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new HL7CustomException(e.getMessage()+"Exception occurred while search in eResearch,  Please contact System Administrator",
					prop1.getProperty("AcknowledgementError"));
		}	
		/*if(patientDataList1.size()>1){
					StringBuffer reslutMapDemg = getMrgVgdbPtDmg(resultMap,false);
					 ctmsDemg=getCTMSDemg(patientDataList1);
					//emailnotification.sendMail(reslutMapDemg.toString(),ctmsDemg.toString());
					pflag=true;
				throw new HL7CustomException(prop1.getProperty("merg_message_multiplerec_CTMS"),
						prop1.getProperty("AcknowledgementReject"));
			}*/

		 if(pflag == true && nflag == true){
			 String studyCoordintaorList = "";
			 if(prop.containsKey("studyCont_search__eResearch_MRN.patientFacilityId")&& 
						!"".equals(prop.getProperty("studyCont_search__eResearch_MRN.patientFacilityId"))){
			String newMrn_studyCoordinator = getStudyCoordinators(resultMap, prop,1);
			String priorMrn_studyCoordinator = getStudyCoordinators(resultMap, prop,2);
			  studyCoordintaorList = newMrn_studyCoordinator +","+priorMrn_studyCoordinator;
			 }
			emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString(),studyCoordintaorList);
			throw new HL7CustomException(prop1.getProperty("chk_bothmrn_message"),
					prop1.getProperty("AcknowledgementReject"));

		} else if(pflag == true){
			// call Study Coordinators method 
			//
			 String studyCoordinator = "";
			 if(prop.containsKey("studyCont_search__eResearch_MRN.patientFacilityId")&& 
						!"".equals(prop.getProperty("studyCont_search__eResearch_MRN.patientFacilityId"))){
			 studyCoordinator = getStudyCoordinators(resultMap, prop,2);
			 }
			emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString(),studyCoordinator);
			 
			throw new HL7CustomException(prop1.getProperty("merg_message_multiplerec_CTMS"),
					prop1.getProperty("AcknowledgementReject"));

		}  else if(nflag == true){
			emailnotification.sendMail(reslutMapDemg.toString(),allCtmsDemg.toString());
			throw new HL7CustomException(prop1.getProperty("chk_newmrn_message"),
					prop1.getProperty("AcknowledgementReject"));
		}


		return true;
	}




	private String isEmpty(String s){
		logger.info("Checking Empty value-->"+s);
		if(s == null || s.length() == 0 || s.trim().equalsIgnoreCase("null")){
			logger.info("Inside isEmpty");
			return "";
		}else{
			return s;
		}
	}


}
