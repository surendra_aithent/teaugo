package com.velos.integration.adt.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.apache.log4j.Logger;

import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.services.GroupIdentifier;
import com.velos.services.OperationException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientSearchResponse;
import com.velos.services.PatientStudy;

import com.velos.services.StudyIdentifier;
import com.velos.services.StudySummary;
import com.velos.services.User;
import com.velos.services.UserIdentifier;
import com.velos.services.UserSearch;
import com.velos.services.UserSearchResults;

public class StudyContacts  {

	/*public static void main(String[] args) throws  HL7CustomException{

		ADTPostProcessorHelper aDTPostProcessorHelper = new ADTPostProcessorHelper();
		UserSearch userSearch = new UserSearch();
		GroupIdentifier groupIdentifier = new GroupIdentifier();

		OrganizationIdentifier organization = new OrganizationIdentifier();
		organization.setSiteName("CTXpress");
		groupIdentifier.setGroupName("");

		userSearch.setFirstName("surendra");
		userSearch.setLastName("M");
		userSearch.setOrganization(organization);
		userSearch.setPageNumber(1);
		userSearch.setPageSize(1000);
		userSearch.setGroup(groupIdentifier);


		System.out.println("UserSearch Object =====>"+userSearch);

		UserSearchResults userSearchResults;
		try {
			userSearchResults = aDTPostProcessorHelper.searchUser(userSearch);
			System.out.println("UserSearchResults =======>>>>>>"+userSearchResults);
		} catch (OperationException_Exception e) {
			e.getCause().getMessage();
			e.printStackTrace();
		}

		System.out.println("UserSearchResults =======>>>>>>"+userSearchResults);

		List<User> user  = userSearchResults.getUser();

		for(User users :user)
		userName = users.getEmail();

	//	userName += userLoginName;







	}*/

	private static Logger logger = Logger.getLogger(StudyContacts.class);
	private ADTPostProcessorHelper aDTPostProcessorHelper;
	Properties prop = new Properties();
	List<PatientDataBean> patientDataList=new ArrayList<PatientDataBean>();
	List<PatientStudy> patientStudiesList,patientStudiesList1=new ArrayList<PatientStudy>();
	String patientId = null;
	String studyContactList = null;
	List<String> studyNumber = new ArrayList<String>();

	public String	getStudyContacts(Map<String, String> resultMap,Properties prop,int searchType) throws OperationException_Exception, HL7CustomException{
		// searchType 1 means search with MRN, 2 means search with  PRIORMRN 

		//Get patient Id to get study numbers
		patientDataList=getPatinetId(resultMap,prop,searchType);

		for(PatientDataBean patientDataBean :patientDataList){
			patientId = patientDataBean.getPatientIdentifier().getPatientId();
			logger.info("Patient Identifier =========>"+patientId);
		}

		try {
			// get study numbers to get study contacts/coordinators
			patientStudiesList = getStudyNumbers(resultMap, prop, patientId);

			if(patientStudiesList == null){
				logger.info("**Get study Contacts method , No Studies for this Patient***");
				return null;
			}else if(patientStudiesList.size() != 0){

				studyContactList =	 getStudyContacts(patientStudiesList,prop);
			}  

		} catch (OperationException_Exception e) {

			System.out.println("No Studies for Patient");
		}

		System.out.println("Study Contact List ======>"+studyContactList);
		return studyContactList ;
	}


	public String getStudyContacts(List<PatientStudy> patientStudiesList,Properties prop) throws OperationException_Exception, HL7CustomException{

		String mail =null,userName=""  ;
		String firstName = "" ;
		String lastName  = "";

		StudyIdentifier studyIdentifier = new StudyIdentifier();
		StudySummary studySummary = new StudySummary();
		List<String> email = new ArrayList<String>();

		//int listSize = patientDataList.size();
		int count=0;


		OrganizationIdentifier organization = new OrganizationIdentifier();
		String org = prop.getProperty("organization");
		System.out.println("\n\nOrganization ========>"+org);
		organization.setSiteName(org.trim().toString());
		if(patientStudiesList.size() > 1){

			logger.info("*****************More than one Study for this patient *************");
			for(PatientStudy patientStudy :patientStudiesList){

				//studyNumber.add( patientStudy.getStudyIdentifier().getStudyNumber());	
				//studyIdentifier.setStudyNumber(studyNumber.get(0));

				studyIdentifier.setStudyNumber(patientStudy.getStudyIdentifier().getStudyNumber());
				studySummary = aDTPostProcessorHelper.getStudySummary(studyIdentifier);
				UserIdentifier userIdentifier =  studySummary.getStudyContact();

				if(userIdentifier != null){
					firstName = userIdentifier.getFirstName();
					lastName  = userIdentifier.getLastName();

					//	logger.info("userLoginName=====> "+userLoginName);
					logger.info("StudyContact firstName=====> "+firstName);
					logger.info("StudyContact lastName=====> "+lastName);

					UserSearch userSearch = new UserSearch();
					GroupIdentifier groupIdentifier = new GroupIdentifier();

					groupIdentifier.setGroupName("");

					userSearch.setFirstName(firstName);
					userSearch.setGroup(groupIdentifier);
					userSearch.setLastName(lastName);
					userSearch.setOrganization(organization);
					userSearch.setPageNumber(1);
					userSearch.setPageSize(100);
					
					//userSearch.setGroup(groupIdentifier);


					System.out.println("UserSearch Object =====>"+userSearch);

					UserSearchResults userSearchResults =aDTPostProcessorHelper.searchUser(userSearch);
					
					if(userSearchResults == null){
						logger.info("userSearchResults  Object null , please check the User is created or not");
						return null;
					}

					System.out.println("UserSearchResults =======>>>>>>"+userSearchResults);

					List<User> user  = userSearchResults.getUser();
					System.out.println("size of the list ===> "+ user.size());
					
					

					for(User users :user){
						mail= users.getEmail();

					if(mail == null || "".equals(mail)){
						return null;
					}

					email.add(mail);
					}
				}

			}
			/*if(userLoginName != null || !"".equals(userLoginName) ){
				if(listSize == count ){
					userName += userLoginName+domainName;
				}
				else{
					userName += userLoginName+domainName+",";
				}}
				count++;*/



			for(String mailId:email){
				if(email.size() == count ){
					userName += mailId;
				}
				else{
					userName += mailId+",";
				}
				count++;}

		} else if(patientStudiesList.size() == 1){

			logger.info("*****************Single Study for this patient *************");

			for(PatientStudy patientStudy :patientStudiesList){
				studyIdentifier.setStudyNumber(patientStudy.getStudyIdentifier().getStudyNumber());
			}

			studySummary = aDTPostProcessorHelper.getStudySummary(studyIdentifier);
			UserIdentifier userIdentifier =  studySummary.getStudyContact();

			//userLoginName = userIdentifier.getUserLoginName();
			firstName = userIdentifier.getFirstName();
			lastName  = userIdentifier.getLastName();

			//	logger.info("userLoginName=====> "+userLoginName);
			logger.info("StudyContact firstName=====> "+firstName);
			logger.info("StudyContact lastName=====> "+lastName);

			UserSearch userSearch = new UserSearch();
			GroupIdentifier groupIdentifier = new GroupIdentifier();

			groupIdentifier.setGroupName("");

			userSearch.setFirstName(firstName);
			userSearch.setGroup(groupIdentifier);
			userSearch.setLastName(lastName);
			userSearch.setOrganization(organization);
			userSearch.setPageNumber(1);
			userSearch.setPageSize(100);
			//userSearch.setGroup(groupIdentifier);


			System.out.println("UserSearch Object =====>"+userSearch);

			UserSearchResults userSearchResults =aDTPostProcessorHelper.searchUser(userSearch);

			System.out.println("UserSearchResults =======>>>>>>"+userSearchResults);

			List<User> user  = userSearchResults.getUser();
			System.out.println("size of the list ===> "+ user.size());
			for(User users :user)
				userName = users.getEmail();

			//	userName += userLoginName;

		}
		logger.info("userLoginName=====> "+userName);
		System.out.println("userLoginName=====> "+userName);

		return userName;
	}
	public  List<PatientStudy> getStudyNumbers(Map<String, String> resultMap,Properties prop,String patientId) throws OperationException_Exception, HL7CustomException{

		//PatientStudy patientStudyList = new PatientStudy();

		PatientIdentifier patientIdentifier = new PatientIdentifier();
		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();

		organizationIdentifier.setSiteName(prop.getProperty("organization"));
		patientIdentifier.setPatientId(patientId);
		//patientIdentifier.setPatientId("008");
		patientIdentifier.setOrganizationId(organizationIdentifier);

		logger.info("PatientIdentifier ============>"+patientId);
		logger.info("OrganizationIdentifier ============>"+organizationIdentifier.getSiteName());
		System.out.println("PatientIdentifier ============>"+patientId);

		try {
			patientStudiesList1 = aDTPostProcessorHelper.getPatientStudies(patientIdentifier);


		} catch (OperationException_Exception e) {

			//throw e;
		}

		//getStudyContacts(patientStudiesList);

		return patientStudiesList1;

	}



	List<PatientDataBean> getPatinetId(Map<String, String> resultMap,Properties prop,int searchType) throws HL7CustomException{
		// searchType 1 means search with MRN, 2 means search with PRIORMRN 
		aDTPostProcessorHelper = new ADTPostProcessorHelper();
		try {
			if(searchType == 1){
				System.out.println("************** MRN Search ********");
				patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "studyCont_search__eResearch_MRN",
								2);
			}else if(searchType == 2){
				System.out.println("************** prior MRN Search ********");
				patientDataList = aDTPostProcessorHelper
						.searchPatientHelper(resultMap, prop, "studyCont_search_eResearch_PriorMRN",
								2);
			}

			logger.info("Patient Data List  for patient id ========>"+patientDataList);

		} catch (HL7CustomException e) {
			throw e;
		}

		return patientDataList;

	}




	/*public String getPatientId(Map<String, String> resultMap,Properties prop) throws OperationException_Exception{

		patientDataList=ctmsSearchPatient(resultMap,prop);

		for(PatientDataBean patientDataBean :patientDataList){

			patientId = patientDataBean.getPatientIdentifier().getPatientId();


			System.out.println("Patient Identifier =========>"+patientId);
		}

		getStudyNumbers(resultMap, prop, patientId);

		return patientId;
	}*/



}
