package com.velos.integration.gateway;

import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.SoapEndpointInterceptor;

public class SimpleInterceptor implements SoapEndpointInterceptor {
	
	private final String WSA_NS = "http://www.w3.org/2005/08/addressing";
	private final String TO_STR = "To";
	private final String Action_STR = "Action";
	
	@Override
	public boolean understands(SoapHeaderElement header) {
		if(header.getName().getNamespaceURI().equals(WSA_NS)) { 
        	if (header.getName().getLocalPart().equals(TO_STR) || header.getName().getLocalPart().equals(Action_STR) ) {
        		return true;
        	}
		}
		return false;
	}

	@Override
	public boolean handleRequest(MessageContext messageContext, Object endpoint)
			throws Exception {
		return true;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext, Object endpoint)
			throws Exception {
		return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext, Object endpoint)
			throws Exception {
		return true;
	}

	@Override
	public void afterCompletion(MessageContext messageContext, Object endpoint,
			Exception ex) throws Exception {
	}

}
