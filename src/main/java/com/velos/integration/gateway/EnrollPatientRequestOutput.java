package com.velos.integration.gateway;

import java.util.ArrayList;
import java.util.Map;

import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class EnrollPatientRequestOutput {
	private static final String outputTemplate = 
			"<EnrollPatientRequestResponse xmlns:ns2=\"urn:ihe:qrph:rpe:2009\" xmlns=\"urn:hl7-org:v3\">"+
					"<patient>"+
					"<candidateID root=\"8.3.5.6.7.78\" extension=\"%s\"></candidateID>"+
					//"<CandidateID root=\"8.3.5.6.7.78\" extension=\"%s\" assigningAuthorityName=\"%s\" />"+
					"<subjectID root=\"8.3.5.6.7.78\" extension=\"%s\"></subjectID>"+
					"<name><given>%s</given><family>%s</family></name>"+
					"<address>"+
					"<streetAddressLine xmlns=\"urn:hl7-org:v3\">%s</streetAddressLine>"
					+ "<city xmlns=\"urn:hl7-org:v3\">%s</city>"
					+ "<state xmlns=\"urn:hl7-org:v3\">%s</state>"
					+ "<postalCode xmlns=\"urn:hl7-org:v3\">%s</postalCode>"
					+ "<country xmlns=\"urn:hl7-org:v3\">%s</country>"
					+ "</address>"+
					"<dob value=\"%s\"></dob>"+
					"</patient>"+
					"<processState>%s</processState>"+
					//"<study><id root=\"8.3.5.6.7.78\" extension=\"%s\" assigningAuthorityName=\"%s\"/></study>"+
					"<study><id root=\"8.3.5.6.7.78\" extension=\"%s\"/></study>"+
					"</EnrollPatientRequestResponse>";

	public String generateOutput(Map<VelosKeys, String> dataMap) {
		ArrayList<String> argList = new ArrayList<String>();
		//argList.add(dataMap.get(ProtocolKeys.PatientId));
		argList.add(dataMap.get(ProtocolKeys.PatientFacilityId));
		argList.add(dataMap.get(ProtocolKeys.StudyPatId));
		argList.add(dataMap.get(ProtocolKeys.FirstName));
		argList.add(dataMap.get(ProtocolKeys.LastName));
		argList.add(dataMap.get(ProtocolKeys.StreetAddressLine));
		argList.add(dataMap.get(ProtocolKeys.City));
		argList.add(dataMap.get(ProtocolKeys.State));
		argList.add(dataMap.get(ProtocolKeys.PostalCode));
		argList.add(dataMap.get(ProtocolKeys.Country));
		argList.add(dataMap.get(ProtocolKeys.Dob));
		argList.add(dataMap.get(ProtocolKeys.CompletedAction));
		argList.add(dataMap.get(ProtocolKeys.StudyNumber));
		//argList.add(dataMap.get(ProtocolKeys.SiteName));
		return String.format(outputTemplate, argList.toArray());
	}
}
