package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.Map;

public class EpicMaps {
	private static final HashMap<String, String> velosToEpicPatStudyStatMap;
	private static final HashMap<String, String> epicToVelosPatStudyStatMap;
	
	static
    {
		velosToEpicPatStudyStatMap = new HashMap<String, String>();
		velosToEpicPatStudyStatMap.put("enrolled", "1055");
		velosToEpicPatStudyStatMap.put("active", "1060");
		velosToEpicPatStudyStatMap.put("offstudy", "1070");
		velosToEpicPatStudyStatMap.put("scrfail", "1080");
		//velosToEpicPatStudyStatMap.put("Withdrawn", "1090");
		velosToEpicPatStudyStatMap.put("followup", "1068");
		//velosToEpicPatStudyStatMap.put("Lost to Follow-Up", "1069");
		//velosToEpicPatStudyStatMap.put("Added in Error", "1041");
		
		epicToVelosPatStudyStatMap = new HashMap<String, String>();
		epicToVelosPatStudyStatMap.put("1055", "enrolled");
		epicToVelosPatStudyStatMap.put("1060", "active");
		epicToVelosPatStudyStatMap.put("1070", "offstudy");
		epicToVelosPatStudyStatMap.put("1080", "scrfail");
		//epicToVelosPatStudyStatMap.put("1090", "Withdrawn");
		epicToVelosPatStudyStatMap.put("1068", "followup");
		//epicToVelosPatStudyStatMap.put("1069", "Lost to Follow-Up");
		//epicToVelosPatStudyStatMap.put("1041", "Added in Error");
    }
	
	public static Map<String, String> getVelosToEpicPatStudyStatusMap() {
		return velosToEpicPatStudyStatMap;
	}
	
	public static Map<String, String> getEpicToVelosPatStudyStatMap() {
		return epicToVelosPatStudyStatMap;
	}
}
