package com.velos.integration.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.epic.XmlProcess;
import com.velos.services.GetStudyPatientStatusHistoryResponse;
import com.velos.services.GetStudyStatusesResponse;
import com.velos.services.GetStudySummaryResponse;
import com.velos.services.NvPair;
import com.velos.services.StudyStatus;
import com.velos.services.StudyStatuses;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class VelosStudyMapper {
	private static Logger logger = Logger.getLogger("epicLogger"); 
	private String endpoint = null;
	private Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
	
	public VelosStudyMapper(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public Map<VelosKeys, Object> mapStudySummary(GetStudySummaryResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(resp);
		}
		return dataMap;
	}
	
	
	public Map<VelosKeys, Object> mapStudyStatuses(GetStudyStatusesResponse resp) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudyStatusesForEpic(resp);
		}
		return dataMap;
	}
	

	private void mapStudyStatusesForEpic(GetStudyStatusesResponse resp) {
		StudyStatuses studyStatues = resp.getStudyStatuses();
		List<StudyStatus>  studyStatusList= studyStatues.getStudyStatus();
		System.out.println("studyStatusList=="+studyStatusList);
		if(studyStatusList!=null && !studyStatusList.isEmpty()){
			for(StudyStatus studyStatus:studyStatusList){
				String code = studyStatus.getStatus().getCode();
				if(code!=null && code.trim().equalsIgnoreCase("ReadyforEpic")){
					dataMap.put(ProtocolKeys.StudyStatuses, code);
				}else{
					//
				}
			}
		}
	}

	public Map<VelosKeys, Object> mapStudySummary(StudySummary summary) {
		if (EndpointKeys.Epic.toString().equals(endpoint)) {
			mapStudySummaryForEpic(summary);
		}
		return dataMap;
	}
	
	private void mapStudySummaryForEpic(GetStudySummaryResponse resp) {
		mapStudySummaryForEpic(resp.getStudySummary());
	}
	
	private void mapStudySummaryForEpic(StudySummary summary) {
		logger.info("\n**********************mapStudySummaryForEpic _ Method*****************\n");
		dataMap.put(ProtocolKeys.Title, MapUtil.escapeSpecial(summary.getStudyTitle()));
		dataMap.put(ProtocolKeys.Text, MapUtil.escapeSpecial(summary.getSummaryText()));
		dataMap.put(ProtocolKeys.IdExtension, MapUtil.escapeSpecial(summary.getStudyNumber()));
		/*List<NvPair> moreList = summary.getMoreStudyDetails();
		for (NvPair more : moreList) {
			if (ProtocolKeys.IrbNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.IrbNumber, more.getValue());
			} else if (ProtocolKeys.Irb.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Irb, more.getValue());
			} else if (ProtocolKeys.NctNumber.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.NctNumber, more.getValue());
			} else if (ProtocolKeys.Nct.toString().equalsIgnoreCase(more.getKey())) {
				dataMap.put(ProtocolKeys.Nct, more.getValue());
			}
		}*/
		         
		       
				 String NctNumber =summary.getNctNumber();
		         
				 if(NctNumber != null){
		        	
					 dataMap.put(ProtocolKeys.NctNumber,NctNumber.substring(3));
		         } else if(NctNumber == null){
		        	 dataMap.put(ProtocolKeys.NctNumber,"");
		         }
		         logger.info("NCTNumber --->"+ NctNumber);
		
		         
		UserIdentifier userIdentifier= summary.getStudyContact();
		System.out.println("userIdentifier=="+userIdentifier);
		if(userIdentifier!=null){
			dataMap.put(ProtocolKeys.LastName, isEmpty(userIdentifier.getLastName()));
			dataMap.put(ProtocolKeys.FirstName, isEmpty(userIdentifier.getFirstName()));
			dataMap.put(ProtocolKeys.StudyContact,isEmpty( userIdentifier.getUserLoginName()));
		
		} else if(userIdentifier == null){
			dataMap.put(ProtocolKeys.LastName, "");
			dataMap.put(ProtocolKeys.FirstName,"");
			dataMap.put(ProtocolKeys.StudyContact,"");
		}
		
		
		UserIdentifier userIdentifier1= summary.getPrincipalInvestigator();
		System.out.println("userIdentifier1=="+userIdentifier1);
		logger.info("userIdentifier1====>"+userIdentifier1);
		
		if(userIdentifier1!=null){
			logger.info("***********UserIdentifer is not null*******");
			dataMap.put(ProtocolKeys.PILastName, isEmpty(userIdentifier1.getLastName()));
			dataMap.put(ProtocolKeys.PIFirstName, isEmpty(userIdentifier1.getFirstName()));
			dataMap.put(ProtocolKeys.PIContact,isEmpty(userIdentifier1.getUserLoginName()));
		} else if(userIdentifier1 == null){
			logger.info("***********UserIdentifer1 is null*******");
			dataMap.put(ProtocolKeys.PILastName,"");
			dataMap.put(ProtocolKeys.PIFirstName, "");
			dataMap.put(ProtocolKeys.PIContact,"");
			
		}
		logger.info("\n**********************mapStudySummaryForEpic _ Method========>\n"+dataMap);
		
	
	}
	
	public String isEmpty(String s){
		   
		if(s == null){
			return s="";
		}
		
		return s;
	   }
}
