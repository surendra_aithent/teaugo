package com.velos.integration.dao;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class MessageDAO {
	
	private static Logger logger = Logger.getLogger("epicLogger");
	
	private DataSource eresDataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setEresDataSource(DataSource eresDataSource) {
		this.eresDataSource = eresDataSource;
		this.jdbcTemplate = new JdbcTemplate(eresDataSource);
		logger.info("*******Jdbc Connection created successfully");	}
	
	public boolean saveMessageDetails(String messageType,String requestXML,String responseXML) {
		System.out.println("saveMessageDetails");
		
		String requestxml=requestXML.replace("\"","");
		String responsexml=responseXML.replace("\"","");

		logger.info("Audit RequestXML "+requestxml);
		logger.info("Audit RespnseXML "+responsexml);
		boolean isSuccess = false;
		try{
			String sql = "INSERT INTO VGDB_RPE_AUDIT_DETAILS"
					+"(pk_msg_audit,MESSAGE_TYPE,REQUEST_MESSAGE,RESPONSE_MESSAGE,CREATION_DATE)"
					+" VALUES(nextval('seq_vgdb_rpe_audit_details'),?,?,?,current_timestamp)";
			jdbcTemplate.update(sql,new Object[]{messageType,requestxml,responsexml});
			isSuccess = true;
			System.out.println("DB Update Success ===>"+isSuccess);
		}catch(Exception e){
			e.printStackTrace();
			isSuccess = false;
		}
		return isSuccess;
	}
	
}
