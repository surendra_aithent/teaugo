package com.velos.integration.adtx.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.velos.integration.hl7.notifications.HL7CustomException;
import com.velos.services.OperationException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientStudySEI")
public interface VelosEspPatientStudyServiceEndpoint {
	@WebResult(name = "PatientStudy", targetNamespace = "")
	@RequestWrapper(localName = "getPatientStudies", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudies")
	@WebMethod
	@ResponseWrapper(localName = "getPatientStudiesResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.GetPatientStudiesResponse")
	
	public List<com.velos.services.PatientStudy> getPatientStudies(
			@WebParam(name = "PatientIdentifier", targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier)
			throws OperationException_Exception, HL7CustomException;

}
