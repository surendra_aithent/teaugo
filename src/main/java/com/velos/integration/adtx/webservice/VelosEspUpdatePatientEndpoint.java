package com.velos.integration.adtx.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;

@WebService(targetNamespace = "http://velos.com/services/", name = "PatientDemographicsSEI")
public interface VelosEspUpdatePatientEndpoint {
	@WebResult(name = "ResponseHolder", targetNamespace = "")
	@RequestWrapper(localName = "updatePatient", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdatePatient")
	@WebMethod
	@ResponseWrapper(localName = "updatePatientResponse", targetNamespace = "http://velos.com/services/", className = "com.velos.services.UpdatePatientResponse")
	public com.velos.services.ResponseHolder updatePatient(
			@WebParam(name = "PatientIdentifier", targetNamespace = "") com.velos.services.PatientIdentifier patientIdentifier,
			@WebParam(name = "PatientDemographics", targetNamespace = "") com.velos.services.UpdatePatientDemographics patientDemographics)
			throws OperationException_Exception,
			OperationRolledBackException_Exception, Exception;
}
