package com.velos.epic;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.epic.service.NOT_StudyProcess;
import com.velos.epic.service.StudyProcess;
import com.velos.integration.dao.MessageDAO;


public class NOTrigger {
	public static String studyId = null;
	public static  String patientId = null;
	private static Logger logger = Logger.getLogger(NOTrigger.class); 
	ClassPathXmlApplicationContext epicContext = null;
	public MessageDAO messageDao =null;
	Properties prop;


	public NOTrigger(){
		prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void sendStudy() throws IOException{
		logger.info("\n\n\n\n\n#***********************Study Data Pushing Without Trigger Started***************************#\n\n");

		epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
		messageDao = (MessageDAO) epicContext.getBean("messageDao");

		NOT_StudyProcess studyProcess = new NOT_StudyProcess();
		String stdCsvFn= prop.getProperty("studyCsvFileName");
		URL url=getClass().getClassLoader().getResource(stdCsvFn);
		CsvReader csvReader = new CsvReader(url.getFile());
		csvReader.readHeaders();

		while(csvReader.readRecord()){
			studyId = csvReader.get("StudyNumber");
			messageDao.saveMessageDetails("Study DataPush",studyId,"Requested Study");
			logger.info("\n\n ###################################\n\nStudyNumber ==============> "+studyId);
			try{
				studyProcess.getStudyDetails(studyId);
				logger.info("\n\n\n***************** Study Creation Completed ====> "+studyId+"*********\n\n\n");
				Thread.sleep(10000);
			}
			catch(Exception e){
				logger.info("\n\n ######################Study Creation Failed  =====> "+studyId+" ########");
				logger.info("Study Exception block");
				logger.info(e.getMessage() +""+e.getCause());
				try{
				String fileLoc = prop.getProperty("failedStudies.loc");
				logger.info("Failed Studies File Location ====>"+fileLoc);
				String errorMessage =e.getMessage();
				if(errorMessage == null){
					errorMessage ="Study Creation faild please check the log";
				}
				failedStudies(studyId,fileLoc,errorMessage);
				}catch(Exception ess){
					logger.info("Study File exception "+ess.getMessage() +" "+ess.getCause());
				}
				
			}
		}
	}

	public void sendEnrollPatientToStudy() throws IOException {
		logger.info("\n\n\n********************EnrollPatientToStudy Data Pushing Started Without Trigger *****************\n\n");

		epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
		messageDao = (MessageDAO) epicContext.getBean("messageDao");

		NOT_StudyProcess studyProcess = null;
		String patstdCsvFn= prop.getProperty("patStudyCsvFileName");
		URL url=getClass().getClassLoader().getResource(patstdCsvFn);

		
			CsvReader csvReader = new CsvReader(url.getFile());
			csvReader.readHeaders();

			while(csvReader.readRecord()){

				try{
				studyProcess = new NOT_StudyProcess();
				studyId = csvReader.get("StudyNumber");
				patientId = csvReader.get("CTMSID");
				messageDao.saveMessageDetails("EnrollPatientToStudy DataPush",studyId+","+patientId,"RequestEnrollPatientToStudy");
				logger.info("\n###EnrollStudyId=========>"+studyId);
				logger.info("\n\n##EnrollPatientId ------->"+patientId);

				studyProcess.getEnrollPatientStudyDetails(studyId, patientId);
				logger.info("StudyId ------->"+studyId);
				logger.info("patientId ------->"+patientId);
				logger.info("\n\n####################### EnrollPatientToStudy Completed ==> "+studyId+" PatientId "+patientId +" #########\n\n");

				Thread.sleep(20000);
				}catch(Exception e){
					logger.info("\n\n##########################\n EnrollPatientToStudyFailed for this study====>"+studyId +"  PatientId==>"+patientId+"####\n\n");
					try{
					String  failedStudies=studyId+","+patientId;
					String fileLoc = prop.getProperty("failedPatientStudies.loc");
					logger.info("EnrollPatient File Location ====>"+fileLoc);
					String errorMessage =e.getMessage();
					if(errorMessage == null){
						errorMessage ="PatientStudyEnrollment  faild please check the log";
					}
					failedStudies(failedStudies,fileLoc,errorMessage);
					logger.info("Error Studies filed");
					System.out.println("Error Studies filed");
					}catch(Exception es){
						logger.info("Enroll File exception "+es.getMessage() +" "+es.getCause());
					}
					
				}

			}
		
		
		/*catch(Exception e){
			logger.info("\n\n##########################\n EnrollPatientToStudyFailed for this study====>"+studyId +"  PatientId==>"+patientId+"####\n\n");
			try{
			String  failedStudies=studyId+","+patientId;
			String fileLoc = prop.getProperty("failedPatientStudies.loc");
			logger.info("EnrollPatient File Location ====>"+fileLoc);
			String errorMessage =e.getMessage();
			if(errorMessage == null){
				errorMessage ="PatientStudyEnrollment  faild please check the log";
			}
			failedStudies(failedStudies,fileLoc,errorMessage);
			logger.info("Error Studies filed");
			System.out.println("Error Studies filed");
			}catch(Exception es){
				logger.info("Enroll File exception "+es.getMessage() +" "+es.getCause());
			}
			
		}*/
	}

	public void failedStudies(String content,String fileLoc,String errorMsg) throws IOException{
		File file = new File(fileLoc);
		FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content+"==>"+errorMsg+"\n");
		bw.close();

	}

	public static void main(String[] args) throws IOException, InterruptedException{

		//sendEnrollPatientToStudy();

	}

}
