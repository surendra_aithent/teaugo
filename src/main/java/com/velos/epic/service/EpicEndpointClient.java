package com.velos.epic.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.addressing.version.Addressing10;
import org.springframework.context.ApplicationContext;

import com.velos.epic.XmlProcess;
import com.velos.integration.dao.MessageDAO;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

@Component
public class EpicEndpointClient {

	private static Logger logger = Logger.getLogger("epicLogger");   
	private static ResourceBundle bundle = ResourceBundle.getBundle("epic-testclient-req");
	private MessageDAO messageDao = null;
    Map<String,String> messageMap=new HashMap<String,String>();
	public String sendRequest(Map<VelosKeys,Object> resultMap) throws OperationCustomException  {
		String requestXml = null;
		String studyId = (String) resultMap.get(ProtocolKeys.StudyId);
		XmlProcess xp=new XmlProcess();
		requestXml=xp.xmlGenerating(studyId);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.RetrieveProtocol, requestXml);*/
		return requestXml;
	}
	public String enrollPatientRequest(Map<VelosKeys,Object> resultMap) throws OperationCustomException{
		XmlProcess xp=new XmlProcess();
		String	requestXml=xp.enrollPatientMessage(resultMap);
	/*	EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.EnrollPatientRequest, requestXml);*/
		return requestXml;
	}
	public String alertProtocolState(Map<VelosKeys, Object> dataMap) throws OperationCustomException{
		XmlProcess xp=new XmlProcess();
		String	requestXml=xp.alertProtocolState(dataMap);
		/*EpicEndpointClient client = new EpicEndpointClient();
		client.handleRequest(ProtocolKeys.AlertProtocolState,requestXml);*/
		return requestXml;
	}
	@SuppressWarnings("resource")
	public String handleRequest(ProtocolKeys request, Map<VelosKeys, Object> dataMap) throws OperationCustomException   {
		/*if (requestXml == null) { return null; }*/
		logger.info("\n"+request+"\n Map Values ====>"+dataMap);
		String body = null;
		String protocol=null;
		StringWriter writer = new StringWriter();
		ClassPathXmlApplicationContext context = null,epicContext = null;
		try{
			context=new ClassPathXmlApplicationContext("epic-testclient-req.xml");
			epicContext = new ClassPathXmlApplicationContext("epic-context.xml");
			messageDao = (MessageDAO) epicContext.getBean("messageDao");
			switch (request) {
			case RetrieveProtocol:
				body=sendRequest(dataMap);
				protocol="RetrieveProtocolDefResponse";
				break;
			case EnrollPatientRequest: 
				System.out.println("EnrollPatientRequest Starts ***********************");
				body= enrollPatientRequest(dataMap);
				protocol="EnrollPatientRequest";
				break;
			case AlertProtocolState:
				body=alertProtocolState(dataMap);
				protocol="AlertProtocolState";
				break;
			default: body = ""; break;
			}
			logger.info("\n\n********** Request Outbound Message *******\n\n"+body);
			WebServiceTemplate webServiceTemplate = (WebServiceTemplate)context.getBean("webServiceTemplate");
			StreamSource source = new StreamSource(new StringReader(body));
			StreamResult result = new StreamResult(writer);
			logger.info(" ************ Message send starts *************");
			webServiceTemplate.sendSourceAndReceiveToResult(source,wasHeader(protocol),result);
			//Auditing Request and Response Message into vgdb_epicmessaging_audit_values table
			messageDao.saveMessageDetails("Outbound",body,writer.toString());
			logger.info("\n\n************ Response Message  *************\n\n"+writer.toString());
			System.out.println("Got this response\n"+writer.toString());
		}catch(OperationCustomException e){
			e.printStackTrace();
			logger.info(" OutboundMessage********OpearationCustomException Block*******"+e.getLocalizedMessage()+"\n"+e.getResponseType());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getResponseType());
			messageDao.saveMessageDetails("Outbound",body,e.getResponseType());
			//throw new OperationCustomException();
		}catch(Exception e){
			System.out.println("EnrollPatientRequest Starts ***********************");
			logger.info("Outbound Message********Exception Block*******"+e.getLocalizedMessage()+"\n"+e.getMessage());
			//messageDao.saveMessageDetails("Outbound",e.getLocalizedMessage(),e.getMessage());
			messageDao.saveMessageDetails("Outbound",body,e.getMessage());
		}finally{
			//((ClassPathXmlApplicationContext) context).close();
			//((ClassPathXmlApplicationContext) epicContext).close();
			logger.info("closed classpath application context");
			System.out.println("closed classpath application context");
			 
		}
		return writer.toString();
	}
	public static String getProperty(String key) {
		return bundle.getString(key);
	}
	@Bean
	public ActionCallback wasHeader(String protocol) throws Exception {
		ActionCallback header = null;
		try {					  
			URI action = new URI("urn:ihe:qrph:rpe:2009:"+protocol);
			Addressing10 version = new Addressing10();
			String scheme = getProperty("epic.scheme");
			String host = getProperty("epic.host");
			String port = getProperty("epic.port");
			String context = getProperty("epic.context");
			if (context != null && !context.startsWith("/")) {
				context = "/" + context;
			}
			URI to = null;
			int portInt = -1;
			try {
				portInt = Integer.parseInt(port);
			} catch(NumberFormatException e) {
				portInt = 80;
			}
			to = (new URL(scheme, host, portInt, context)).toURI();
			header = new ActionCallback(action, version, to);
		} catch(Exception e) {
			logger.info("Error Response in wasHeaderMethod -->"+ e.getMessage());
			e.printStackTrace();
			throw new OperationCustomException(e.getMessage());
		}
		return header;
	}
}





