package com.velos.epic.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.integration.core.processor.StudyStatusMessageProcessor;
import com.velos.integration.espclient.VelosEspClient;
import com.velos.integration.espclient.VelosEspMethods;
import com.velos.integration.hl7.notifications.OperationCustomException;
import com.velos.integration.mapping.EndpointKeys;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;

public class StudyProcess {
	private static Logger logger = Logger.getLogger("epicLogger");
	
	
	Map<VelosKeys,String> resultMap=new HashMap<VelosKeys,String>();
	EpicEndpointClient eepc=new EpicEndpointClient();
	
	//get study details for outbound RetriveProtocolDef Response service
	public void getStudyDetails(String studyNumber){
		EpicEndpointClient eepc=new EpicEndpointClient();
		logger.info("****************RetriveProtocolDef Response Starts **********************");
		Map<VelosKeys, Object> dataMap = new HashMap<VelosKeys, Object>();
		try {
			dataMap.put(ProtocolKeys.StudyId,studyNumber);
			eepc.handleRequest(ProtocolKeys.RetrieveProtocol,dataMap);
			//eepc.sendRequest(studyNumber);
			logger.info("****************RetriveProtocolDef Response Ends **********************");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	//get EnrollPatienttoStudy Details for outbound Service
	public void getEnrollPatientStudyDetails(String studyId, String patientId) throws OperationCustomException{
		logger.info(" **************EnrollPatientStudy Starts ******************");
		//resultMap= new HashMap<VelosKeys,String>();
		 if(studyId != null && patientId != null){
		resultMap.put(ProtocolKeys.StudyId,studyId);
		resultMap.put(ProtocolKeys.PatientID, patientId);
		logger.info("StudyId--->"+studyId);
		logger.info("*******************Incoming Values ---->"+resultMap);

		VelosEspClient client = new VelosEspClient();
		resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
		
		Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
		
		resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
		
		Map<VelosKeys, Object> dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
        
        resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
        System.out.println("StudyPatStatus="+dataMap1.get(ProtocolKeys.StudyPatStatus));
        
        Map<VelosKeys, Object> dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
		logger.info("Patient Data --->"+dataMap.isEmpty());
		logger.info(dataMap);
		dataMap.put(ProtocolKeys.StudyId,studyId);
		dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
		dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));
		//eepc.enrollPatientRequest(dataMap);
		eepc.handleRequest(ProtocolKeys.EnrollPatientRequest,dataMap);
		 }
		 else{
			 logger.info("StudyId or PatientId should not null");
		 }
		 logger.info(" **************EnrollPatientStudy Ends ******************");
	}
	public void getAlertProtocolState(String studyId, String patientId) throws OperationCustomException{
		logger.info(" **************AlertProtocolState Starts ******************");
		 if(studyId != null && patientId != null){
		resultMap.put(ProtocolKeys.StudyId,studyId);
		resultMap.put(ProtocolKeys.PatientID, patientId);
		logger.info("StudyId--->"+studyId);
		logger.info("PatientId--->"+patientId);
		logger.info("Incoming Values ---->"+resultMap);
		VelosEspClient client = new VelosEspClient();
		resultMap.put(EndpointKeys.Endpoint, EndpointKeys.Epic.toString());
		Map<VelosKeys, Object> dataMap = client.handleRequest(VelosEspMethods.PatientDetails,resultMap);
		resultMap.put(ProtocolKeys.OID,(String) dataMap.get(ProtocolKeys.OID));
		Map<VelosKeys, Object> dataMap1 = client.handleRequest(VelosEspMethods.PatientStudyGetStudyPatientStatusHistory,resultMap);
        resultMap.put(ProtocolKeys.StudyPatOID,(String) dataMap1.get(ProtocolKeys.OID));	
        Map<VelosKeys, Object> dataMap2 = client.handleRequest(VelosEspMethods.StudyPatientStatus,resultMap);
		System.out.println("Patient Data --->"+dataMap.isEmpty());
		System.out.println(dataMap);
		dataMap.put(ProtocolKeys.StudyId,studyId);
		dataMap.put(ProtocolKeys.StudyPatStatus,dataMap1.get(ProtocolKeys.StudyPatStatus));
		dataMap.put(ProtocolKeys.StudyPatId,dataMap2.get(ProtocolKeys.StudyPatId));
		eepc.handleRequest(ProtocolKeys.AlertProtocolState,dataMap);
		//eepc.alertProtocolState(dataMap);
		 }
		 else{
			 logger.info("AlertProtocolState StudyId or PatientId should not null");
		 }
		logger.info(" **************AlertProtocolState ends ******************");
	}
}
