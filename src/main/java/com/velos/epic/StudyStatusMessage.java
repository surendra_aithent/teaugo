package com.velos.epic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.util.MessageQuery.Result;

import com.velos.integration.hl7.util.StringToXMLGregorianCalendar;
import com.velos.integration.mapping.ProtocolKeys;
import com.velos.integration.mapping.VelosKeys;
import com.velos.services.Code;
import com.velos.services.CompletedAction;
import com.velos.services.CrudAction;
import com.velos.services.EnrollPatientToStudyResponse;
import com.velos.services.OperationException_Exception;
import com.velos.services.OperationRolledBackException_Exception;
import com.velos.services.OrganizationIdentifier;
import com.velos.services.Patient;
import com.velos.services.PatientDataBean;
import com.velos.services.PatientDemographics;
import com.velos.services.PatientEnrollmentDetails;
import com.velos.services.PatientIdentifier;
import com.velos.services.PatientSearch;
import com.velos.services.PatientSearchResponse;
import com.velos.services.PatientStudyStatusIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.Results;
import com.velos.services.Study;
import com.velos.services.StudyIdentifier;
import com.velos.services.StudyOrganization;
import com.velos.services.StudyStatus;
import com.velos.services.StudySummary;
import com.velos.services.UserIdentifier;

public class StudyStatusMessage extends VelosEspStudyPatientEndpoints
{
	private static Logger logger = Logger.getLogger("epicLogger");   
	
	/*private CamelContext context;
	
	public StudyStatusMessage() {
		configureCamel();
	}
	public CamelContext getContext() {
		return context;
	}

	public void setContext(CamelContext context) {
		this.context = context;
	}

	private CamelContext configureCamel() {
		if (this.context == null) {
			@SuppressWarnings("resource")
			ClassPathXmlApplicationContext springContext = new ClassPathXmlApplicationContext(
					"camel-config.xml");
			this.setContext((CamelContext) springContext.getBean("camel"));
		}
		return getContext();
	}
	
	*/

	
	public Map<String,String> studyRequest(String studyId) {
		Map<String,String> resultMap=new HashMap<String,String>();
		
		try {
			StudyIdentifier studyIdentifier = new StudyIdentifier();
			studyIdentifier.setStudyNumber(studyId);
		/*	//Get Study Patient Status
			PatientStudyStatusIdentifier pssId=new PatientStudyStatusIdentifier();
			pssId.setPK(1404); // PK value from Er_PATSTUDYSTATUS DB table
		    PatientEnrollmentDetails ped=getStudyPatientStatus(pssId);
		    ped.getEnrolledBy().getFirstName();
		   */ 
			System.out.println("Get sutdy Summary");
			logger.info("Get Study");
			//*************************************************************
			// Get Study Summary Details
			/*StudySummary ss=getStudySummary(studyIdentifier);
			System.out.println("Study Number --->"+ss.getStudyNumber());
			//System.out.println("NCT_Number"+ss.set);
			resultMap.put("StudyNumber", ss.getStudyNumber());
			*/
			
			//*************************************************************
			//Get Study Details
			studyIdentifier.setStudyNumber(studyId);
			Study study=getStudy(studyIdentifier);
			String studyTitle=study.getStudySummary().getStudyTitle();
			logger.info("Study Title --->"+studyTitle);
			resultMap.put("StudyTitle", studyTitle);
			
		
			
		/*	String nctNumber=study.getNctNumber();
            resultMap.put("NctNumber",nctNumber);   */                       			
			
			//String studyType = study.getStudySummary().getStudyType().getCode();
			//List<StudyStatus> listudystatus=study.getStudyStatuses().getStudyStatus();
			List<StudyStatus> listudystatus=study.getStudyStatuses().getStudyStatus();
			/*for(StudyStatus stst:listudystatus){
				System.out.println("Status --->"+stst.getStatus().getCode());
				logger.info("Status --->"+stst.getStatus().getCode());
			}*/
			

			
			System.out.println("Size --->"+listudystatus.size());
			/*for(int i=0;i<listudystatus.size();i++){
				System.out.println("Study Status "+i+" ---->"+ listudystatus.get(i).getStatus().getCode());
				
				logger.info("Study Status "+i+" ---->"+ listudystatus.get(i).getStatus().getCode());
				
			}*/
			List<StudyOrganization> listorg=study.getStudyOrganizations().getStudyOrganizaton();
			System.out.println("Study Summary Text -- >"+study.getStudySummary().getSummaryText());
			logger.info("Study Summary Text -- >"+study.getStudySummary().getSummaryText());
			
			resultMap.put("StudySummaryText",study.getStudySummary().getSummaryText());
			
			//System.out.println("NationalSampleSize --->"+study.getStudySummary().getNationalSampleSize());
			System.out.println("Study Number -->"+study.getStudyStatuses().getParentIdentifier().getStudyNumber());
			logger.info("Study Number -->"+study.getStudyStatuses().getParentIdentifier().getStudyNumber());
			//System.out.println(study.getStudySummary().getSponsorOtherInfo());
			//study.getStudySummary().getDivision().getCode()
			for(int i=0;i<listorg.size();i++){
				System.out.println("Study Organization - "+i+" ---->"+ listorg.get(i).getOrganizationIdentifier().getSiteName());
			}
			System.out.println("StudyTitle ----->"+ studyTitle);
			
			logger.info("Study Title ---->"+studyTitle);
			
			String studyStatus=listudystatus.get(0).getStatus().getCode();
			System.out.println("Study Status -->"+studyStatus);
			resultMap.put("studyStatus",studyStatus);
			logger.info("Study Status -->"+listudystatus.get(0).getStatus().getCode());
			
			String studyNumber=study.getStudySummary().getStudyNumber();
			logger.info("StudyNumber-->"+studyNumber);
			resultMap.put("studyNumber",studyNumber);
			
			
			
			logger.info("\n\n\n\n\n****************************************************\n\n\n\nResultMap \n"+resultMap);
			logger.info("\n\n************************************\n\n");
			//*************************************************************
			//Enroll Patient to Study Details
			
System.out.println("ResultMap --->"+resultMap);
			
		
			
		} catch (Exception e) {
			/*
			System.out.println("Exception block in study status message class");
             e.getCause().getMessage();

			e.printStackTrace();*/
		}
		return resultMap;
	}
	

	public void createEnrollPatient(Map<String, String>resultMap){

		
		
		String studyNumber=resultMap.get("studyNumber");
		String patientId=resultMap.get("patientId");
		String firstName=resultMap.get("firstName");
		String lastName=resultMap.get("lastName");
		String dob1=resultMap.get("dob");

		
		try{

			PatientIdentifier pid=new PatientIdentifier();


			OrganizationIdentifier orgId=new OrganizationIdentifier();
			orgId.setSiteName("Aithent");
			pid.setOrganizationId(orgId);
			pid.setPatientId(studyNumber);
			XMLGregorianCalendar dob= StringToXMLGregorianCalendar.convertStringDateToXmlGregorianCalendar(dob1,"yyyy-mm-dd",false);


			UserIdentifier userIdentifier=new UserIdentifier();

			//PatientDemographics Details
			PatientDemographics ptdmg = new PatientDemographics();
			ptdmg.setDateOfBirth(dob);
			ptdmg.setFirstName(firstName);
			ptdmg.setLastName(lastName);
			Code code=new Code();
			code.setCode("male");
			ptdmg.setGender(code);
			ptdmg.setPatientCode(patientId);
			Code code2 =new Code();
			code2.setCode("A");
			System.out.println("Code -->"+ code2.getCode());
			ptdmg.setSurvivalStatus(code2);
			ptdmg.setOrganization(orgId);

			Patient patient=new Patient();
			patient.setPatientDemographics(ptdmg);


			StudyIdentifier studyIdentifier = new StudyIdentifier();
			studyIdentifier.setStudyNumber(studyNumber);

			PatientEnrollmentDetails peds =new PatientEnrollmentDetails();
			userIdentifier.setFirstName(firstName);
			userIdentifier.setLastName(lastName);
			userIdentifier.setOrganizationIdentifier(orgId);
			userIdentifier.setUserLoginName("velosadmin");
			peds.setEnrolledBy(userIdentifier);
			Code code1 =new Code();
			code1.setCode("enrolled");
			peds.setStatus(code1);
			XMLGregorianCalendar statusdate= StringToXMLGregorianCalendar.convertStringDateToXmlGregorianCalendar(dob1,"yyyy-mm-dd",false);
			System.out.println("Date Format --->"+statusdate);
			peds.setStatusDate(statusdate);
			
			System.out.println("*********Create&EnrollPatientStarts*******");
			
			createAndEnrollPatient(patient, studyIdentifier, peds);

			System.out.println("C*********reate&EnrollPatient Ends********");
			
		//	enrollPatientToStudy(pid,studyIdentifier1,peds);

		}catch(Exception e){

		}

	}
	
	public void enrollPatientStudyHelper(Map<String, String>resultMap){
		
		String studyNumber=resultMap.get("studyNumber");
		String patientId=resultMap.get("patientId");
		String firstName=resultMap.get("firstName");
		String lastName=resultMap.get("lastName");
		String dob1=resultMap.get("dob");

		//Patient Id details
		PatientIdentifier pid=new PatientIdentifier();
		OrganizationIdentifier orgId=new OrganizationIdentifier();
		orgId.setSiteName("Aithent");
		pid.setOrganizationId(orgId);
		pid.setPatientId(patientId);
		
		//PatientEnrollmentDetails
		PatientEnrollmentDetails peds =new PatientEnrollmentDetails();
		UserIdentifier userIdentifier=new UserIdentifier();
		userIdentifier.setFirstName(firstName);
		userIdentifier.setLastName(lastName);
		userIdentifier.setOrganizationIdentifier(orgId);
		userIdentifier.setUserLoginName("velosadmin");
		peds.setEnrolledBy(userIdentifier);

		Code code1 =new Code();
		code1.setCode("enrolled");
		peds.setStatus(code1);

		XMLGregorianCalendar statusdate= StringToXMLGregorianCalendar.convertStringDateToXmlGregorianCalendar(dob1,"yyyy-mm-dd",false);

		System.out.println("Date Format --->"+statusdate);

		peds.setStatusDate(statusdate);
		
		//StudyNumberDetails
		StudyIdentifier studyIdentifier1 = new StudyIdentifier();
		studyIdentifier1.setStudyNumber(studyNumber);
		
		try {
			System.out.println("***********EnrollPatient Starts*************");
			enrollPatientToStudy(pid,studyIdentifier1,peds);
			System.out.println("***********EnrollPatient Ends*************");
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationRolledBackException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	public List<PatientDataBean> searchPatientHelper(Map<String, String>resultMap){
		
		
		String patientId=resultMap.get("patientId");
		//String firstName=resultMap.get("firstName");
		//String lastName=resultMap.get("lastName");
		//String dob=resultMap.get("dob");
		
		PatientSearch patientSearch = new PatientSearch();
		PatientSearchResponse patientSearchResponse = null;
		
		OrganizationIdentifier orgId = new OrganizationIdentifier();
		orgId.setSiteName("Aithent");
		patientSearch.setPatOrganization(orgId);
		patientSearch.setPatientID(patientId);
		/*if(firstName != null){
		patientSearch.setPatFirstName(firstName);
		}
		if(lastName != null){
		patientSearch.setPatLastName(lastName);
		}
		if(dob1 !=null){
			patientSearch.setPatLastName(dob1);
		}*/
		  try {
			  patientSearchResponse=searchPatient( patientSearch);
		} catch (OperationException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return patientSearchResponse.getPatDataBean();
	}

	
public void	setPatientSearchFilters(PatientSearch patientSearch){
	
	/*patientSearch.setPatientID("");
	patientSearch.setPatFirstName(value);
	patientSearch.setPatLastName(value);
	patientSearch.setPatDateofBirth(value);
	
	patientSearch.setRace(value);*/
		
	}
	
	
	
	}
	


