
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mPatientSchedules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mPatientSchedules">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="mPatientSchedule" type="{http://velos.com/services/}mPatientSchedule" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mPatientSchedules", propOrder = {
    "mPatientSchedule"
})
public class MPatientSchedules
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<MPatientSchedule> mPatientSchedule;

    /**
     * Gets the value of the mPatientSchedule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mPatientSchedule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMPatientSchedule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MPatientSchedule }
     * 
     * 
     */
    public List<MPatientSchedule> getMPatientSchedule() {
        if (mPatientSchedule == null) {
            mPatientSchedule = new ArrayList<MPatientSchedule>();
        }
        return this.mPatientSchedule;
    }

}
