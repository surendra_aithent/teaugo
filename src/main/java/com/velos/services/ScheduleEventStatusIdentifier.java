
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for scheduleEventStatusIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleEventStatusIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="eventIdentifier" type="{http://velos.com/services/}eventIdentifier" minOccurs="0"/>
 *         &lt;element name="scheduleEventStatus" type="{http://velos.com/services/}scheduleEventStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleEventStatusIdentifier", propOrder = {
    "eventIdentifier",
    "scheduleEventStatus"
})
public class ScheduleEventStatusIdentifier
    extends ServiceObject
{

    protected EventIdentifier eventIdentifier;
    protected ScheduleEventStatus scheduleEventStatus;

    /**
     * Gets the value of the eventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventIdentifier }
     *     
     */
    public EventIdentifier getEventIdentifier() {
        return eventIdentifier;
    }

    /**
     * Sets the value of the eventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventIdentifier }
     *     
     */
    public void setEventIdentifier(EventIdentifier value) {
        this.eventIdentifier = value;
    }

    /**
     * Gets the value of the scheduleEventStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleEventStatus }
     *     
     */
    public ScheduleEventStatus getScheduleEventStatus() {
        return scheduleEventStatus;
    }

    /**
     * Sets the value of the scheduleEventStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleEventStatus }
     *     
     */
    public void setScheduleEventStatus(ScheduleEventStatus value) {
        this.scheduleEventStatus = value;
    }

}
