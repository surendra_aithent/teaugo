
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fieldType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="fieldType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="COMMENT"/>
 *     &lt;enumeration value="EDIT_BOX"/>
 *     &lt;enumeration value="MULTIPLE_CHOICE"/>
 *     &lt;enumeration value="HORIZONTAL_LINE"/>
 *     &lt;enumeration value="SPACER"/>
 *     &lt;enumeration value="FORM_LINKS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "fieldType")
@XmlEnum
public enum FieldType {

    COMMENT,
    EDIT_BOX,
    MULTIPLE_CHOICE,
    HORIZONTAL_LINE,
    SPACER,
    FORM_LINKS;

    public String value() {
        return name();
    }

    public static FieldType fromValue(String v) {
        return valueOf(v);
    }

}
