
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormDesign" type="{http://velos.com/services/}linkedFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientFormDesignResponse", propOrder = {
    "formDesign"
})
public class GetPatientFormDesignResponse {

    @XmlElement(name = "FormDesign")
    protected LinkedFormDesign formDesign;

    /**
     * Gets the value of the formDesign property.
     * 
     * @return
     *     possible object is
     *     {@link LinkedFormDesign }
     *     
     */
    public LinkedFormDesign getFormDesign() {
        return formDesign;
    }

    /**
     * Sets the value of the formDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkedFormDesign }
     *     
     */
    public void setFormDesign(LinkedFormDesign value) {
        this.formDesign = value;
    }

}
