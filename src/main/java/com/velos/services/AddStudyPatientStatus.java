
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addStudyPatientStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addStudyPatientStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientStudyStatus" type="{http://velos.com/services/}patientEnrollmentDetails" minOccurs="0"/>
 *         &lt;element name="PatientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addStudyPatientStatus", propOrder = {
    "patientStudyStatus",
    "patientIdentifier",
    "studyIdentifier"
})
public class AddStudyPatientStatus {

    @XmlElement(name = "PatientStudyStatus")
    protected PatientEnrollmentDetails patientStudyStatus;
    @XmlElement(name = "PatientIdentifier")
    protected PatientIdentifier patientIdentifier;
    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the patientStudyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public PatientEnrollmentDetails getPatientStudyStatus() {
        return patientStudyStatus;
    }

    /**
     * Sets the value of the patientStudyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public void setPatientStudyStatus(PatientEnrollmentDetails value) {
        this.patientStudyStatus = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
