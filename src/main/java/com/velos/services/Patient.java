
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patient">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patientDemographics" type="{http://velos.com/services/}patientDemographics" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="patientOrganizations" type="{http://velos.com/services/}patientOrganizations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patient", propOrder = {
    "patientDemographics",
    "patientIdentifier",
    "patientOrganizations"
})
public class Patient
    extends ServiceObject
{

    protected PatientDemographics patientDemographics;
    protected PatientIdentifier patientIdentifier;
    protected PatientOrganizations patientOrganizations;

    /**
     * Gets the value of the patientDemographics property.
     * 
     * @return
     *     possible object is
     *     {@link PatientDemographics }
     *     
     */
    public PatientDemographics getPatientDemographics() {
        return patientDemographics;
    }

    /**
     * Sets the value of the patientDemographics property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientDemographics }
     *     
     */
    public void setPatientDemographics(PatientDemographics value) {
        this.patientDemographics = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the patientOrganizations property.
     * 
     * @return
     *     possible object is
     *     {@link PatientOrganizations }
     *     
     */
    public PatientOrganizations getPatientOrganizations() {
        return patientOrganizations;
    }

    /**
     * Sets the value of the patientOrganizations property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientOrganizations }
     *     
     */
    public void setPatientOrganizations(PatientOrganizations value) {
        this.patientOrganizations = value;
    }

}
