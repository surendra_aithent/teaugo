
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyPatientFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyPatientFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormDesign" type="{http://velos.com/services/}studyPatientFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyPatientFormDesignResponse", propOrder = {
    "formDesign"
})
public class GetStudyPatientFormDesignResponse {

    @XmlElement(name = "FormDesign")
    protected StudyPatientFormDesign formDesign;

    /**
     * Gets the value of the formDesign property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientFormDesign }
     *     
     */
    public StudyPatientFormDesign getFormDesign() {
        return formDesign;
    }

    /**
     * Sets the value of the formDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientFormDesign }
     *     
     */
    public void setFormDesign(StudyPatientFormDesign value) {
        this.formDesign = value;
    }

}
