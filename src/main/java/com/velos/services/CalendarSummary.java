
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for calendarSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarSummary">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="calendarDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calendarDuration" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="calendarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calendarStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="calendarType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}parentIdentifier" minOccurs="0"/>
 *         &lt;element name="statusChangedBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="statusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="statusNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarSummary", propOrder = {
    "calendarDescription",
    "calendarDuration",
    "calendarName",
    "calendarStatus",
    "calendarType",
    "parentIdentifier",
    "statusChangedBy",
    "statusDate",
    "statusNotes"
})
public class CalendarSummary
    extends ServiceObject
{

    protected String calendarDescription;
    protected Duration calendarDuration;
    protected String calendarName;
    protected Code calendarStatus;
    protected Code calendarType;
    protected ParentIdentifier parentIdentifier;
    protected UserIdentifier statusChangedBy;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusDate;
    protected String statusNotes;

    /**
     * Gets the value of the calendarDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarDescription() {
        return calendarDescription;
    }

    /**
     * Sets the value of the calendarDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarDescription(String value) {
        this.calendarDescription = value;
    }

    /**
     * Gets the value of the calendarDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getCalendarDuration() {
        return calendarDuration;
    }

    /**
     * Sets the value of the calendarDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setCalendarDuration(Duration value) {
        this.calendarDuration = value;
    }

    /**
     * Gets the value of the calendarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalendarName() {
        return calendarName;
    }

    /**
     * Sets the value of the calendarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalendarName(String value) {
        this.calendarName = value;
    }

    /**
     * Gets the value of the calendarStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCalendarStatus() {
        return calendarStatus;
    }

    /**
     * Sets the value of the calendarStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCalendarStatus(Code value) {
        this.calendarStatus = value;
    }

    /**
     * Gets the value of the calendarType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCalendarType() {
        return calendarType;
    }

    /**
     * Sets the value of the calendarType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCalendarType(Code value) {
        this.calendarType = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ParentIdentifier }
     *     
     */
    public ParentIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentIdentifier }
     *     
     */
    public void setParentIdentifier(ParentIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the statusChangedBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStatusChangedBy() {
        return statusChangedBy;
    }

    /**
     * Sets the value of the statusChangedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStatusChangedBy(UserIdentifier value) {
        this.statusChangedBy = value;
    }

    /**
     * Gets the value of the statusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusDate() {
        return statusDate;
    }

    /**
     * Sets the value of the statusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusDate(XMLGregorianCalendar value) {
        this.statusDate = value;
    }

    /**
     * Gets the value of the statusNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusNotes() {
        return statusNotes;
    }

    /**
     * Sets the value of the statusNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusNotes(String value) {
        this.statusNotes = value;
    }

}
