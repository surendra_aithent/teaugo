
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOfStudyPatientFormResponsesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOfStudyPatientFormResponsesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyPatientFormResponse" type="{http://velos.com/services/}studyPatientFormResponses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOfStudyPatientFormResponsesResponse", propOrder = {
    "studyPatientFormResponse"
})
public class GetListOfStudyPatientFormResponsesResponse {

    @XmlElement(name = "StudyPatientFormResponse")
    protected StudyPatientFormResponses studyPatientFormResponse;

    /**
     * Gets the value of the studyPatientFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientFormResponses }
     *     
     */
    public StudyPatientFormResponses getStudyPatientFormResponse() {
        return studyPatientFormResponse;
    }

    /**
     * Sets the value of the studyPatientFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientFormResponses }
     *     
     */
    public void setStudyPatientFormResponse(StudyPatientFormResponses value) {
        this.studyPatientFormResponse = value;
    }

}
