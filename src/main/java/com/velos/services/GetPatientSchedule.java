
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientSchedule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScheduleIdentifier" type="{http://velos.com/services/}patientProtocolIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientSchedule", propOrder = {
    "scheduleIdentifier"
})
public class GetPatientSchedule {

    @XmlElement(name = "ScheduleIdentifier")
    protected PatientProtocolIdentifier scheduleIdentifier;

    /**
     * Gets the value of the scheduleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public PatientProtocolIdentifier getScheduleIdentifier() {
        return scheduleIdentifier;
    }

    /**
     * Sets the value of the scheduleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientProtocolIdentifier }
     *     
     */
    public void setScheduleIdentifier(PatientProtocolIdentifier value) {
        this.scheduleIdentifier = value;
    }

}
