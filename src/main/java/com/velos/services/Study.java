
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for study complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="study">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="studyOrganizations" type="{http://velos.com/services/}studyOrganizations" minOccurs="0"/>
 *         &lt;element name="studyStatuses" type="{http://velos.com/services/}studyStatuses" minOccurs="0"/>
 *         &lt;element name="studySummary" type="{http://velos.com/services/}studySummary" minOccurs="0"/>
 *         &lt;element name="studyTeamMembers" type="{http://velos.com/services/}studyTeamMembers" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "study", propOrder = {
    "studyIdentifier",
    "studyOrganizations",
    "studyStatuses",
    "studySummary",
    "studyTeamMembers"
})
public class Study
    extends ServiceObject
{

    protected StudyIdentifier studyIdentifier;
    protected StudyOrganizations studyOrganizations;
    protected StudyStatuses studyStatuses;
    protected StudySummary studySummary;
    protected StudyTeamMembers studyTeamMembers;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the studyOrganizations property.
     * 
     * @return
     *     possible object is
     *     {@link StudyOrganizations }
     *     
     */
    public StudyOrganizations getStudyOrganizations() {
        return studyOrganizations;
    }

    /**
     * Sets the value of the studyOrganizations property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyOrganizations }
     *     
     */
    public void setStudyOrganizations(StudyOrganizations value) {
        this.studyOrganizations = value;
    }

    /**
     * Gets the value of the studyStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link StudyStatuses }
     *     
     */
    public StudyStatuses getStudyStatuses() {
        return studyStatuses;
    }

    /**
     * Sets the value of the studyStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyStatuses }
     *     
     */
    public void setStudyStatuses(StudyStatuses value) {
        this.studyStatuses = value;
    }

    /**
     * Gets the value of the studySummary property.
     * 
     * @return
     *     possible object is
     *     {@link StudySummary }
     *     
     */
    public StudySummary getStudySummary() {
        return studySummary;
    }

    /**
     * Sets the value of the studySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudySummary }
     *     
     */
    public void setStudySummary(StudySummary value) {
        this.studySummary = value;
    }

    /**
     * Gets the value of the studyTeamMembers property.
     * 
     * @return
     *     possible object is
     *     {@link StudyTeamMembers }
     *     
     */
    public StudyTeamMembers getStudyTeamMembers() {
        return studyTeamMembers;
    }

    /**
     * Sets the value of the studyTeamMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyTeamMembers }
     *     
     */
    public void setStudyTeamMembers(StudyTeamMembers value) {
        this.studyTeamMembers = value;
    }

}
