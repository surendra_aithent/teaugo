
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormDesign" type="{http://velos.com/services/}studyFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyFormDesignResponse", propOrder = {
    "formDesign"
})
public class GetStudyFormDesignResponse {

    @XmlElement(name = "FormDesign")
    protected StudyFormDesign formDesign;

    /**
     * Gets the value of the formDesign property.
     * 
     * @return
     *     possible object is
     *     {@link StudyFormDesign }
     *     
     */
    public StudyFormDesign getFormDesign() {
        return formDesign;
    }

    /**
     * Sets the value of the formDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyFormDesign }
     *     
     */
    public void setFormDesign(StudyFormDesign value) {
        this.formDesign = value;
    }

}
