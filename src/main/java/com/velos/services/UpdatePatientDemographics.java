
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePatientDemographics complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePatientDemographics">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}patientDemographics">
 *       &lt;sequence>
 *         &lt;element name="reasonForChange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePatientDemographics", propOrder = {
    "reasonForChange"
})
public class UpdatePatientDemographics
    extends PatientDemographics
{

    protected String reasonForChange;

    /**
     * Gets the value of the reasonForChange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChange() {
        return reasonForChange;
    }

    /**
     * Sets the value of the reasonForChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChange(String value) {
        this.reasonForChange = value;
    }

}
