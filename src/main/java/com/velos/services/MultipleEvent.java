
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for multipleEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multipleEvent">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="eventAttributes" type="{http://velos.com/services/}eventAttributes" minOccurs="0"/>
 *         &lt;element name="eventIdentifier" type="{http://velos.com/services/}eventIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multipleEvent", propOrder = {
    "eventAttributes",
    "eventIdentifier"
})
public class MultipleEvent
    extends ServiceObject
{

    protected EventAttributes eventAttributes;
    protected EventIdentifier eventIdentifier;

    /**
     * Gets the value of the eventAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link EventAttributes }
     *     
     */
    public EventAttributes getEventAttributes() {
        return eventAttributes;
    }

    /**
     * Sets the value of the eventAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventAttributes }
     *     
     */
    public void setEventAttributes(EventAttributes value) {
        this.eventAttributes = value;
    }

    /**
     * Gets the value of the eventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventIdentifier }
     *     
     */
    public EventIdentifier getEventIdentifier() {
        return eventIdentifier;
    }

    /**
     * Sets the value of the eventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventIdentifier }
     *     
     */
    public void setEventIdentifier(EventIdentifier value) {
        this.eventIdentifier = value;
    }

}
