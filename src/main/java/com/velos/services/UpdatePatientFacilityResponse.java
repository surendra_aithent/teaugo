
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePatientFacilityResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePatientFacilityResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updatePatientFacility" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePatientFacilityResponse", propOrder = {
    "updatePatientFacility"
})
public class UpdatePatientFacilityResponse {

    protected ResponseHolder updatePatientFacility;

    /**
     * Gets the value of the updatePatientFacility property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getUpdatePatientFacility() {
        return updatePatientFacility;
    }

    /**
     * Sets the value of the updatePatientFacility property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setUpdatePatientFacility(ResponseHolder value) {
        this.updatePatientFacility = value;
    }

}
