
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for patientOrganization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientOrganization">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="access" type="{http://velos.com/services/}accessRights" minOccurs="0"/>
 *         &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="emptyRegistrationDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="facilityID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="organizationID" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="otherProvider" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="provider" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="registrationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="specialtyAccess" type="{http://velos.com/services/}specialityAccess" minOccurs="0"/>
 *         &lt;element name="systemId" type="{http://velos.com/services/}simpleIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientOrganization", propOrder = {
    "access",
    "_default",
    "emptyRegistrationDate",
    "facilityID",
    "organizationID",
    "otherProvider",
    "provider",
    "registrationDate",
    "specialtyAccess",
    "systemId"
})
public class PatientOrganization
    extends ServiceObject
{

    protected AccessRights access;
    @XmlElement(name = "default")
    protected boolean _default;
    protected boolean emptyRegistrationDate;
    protected String facilityID;
    protected OrganizationIdentifier organizationID;
    protected String otherProvider;
    protected UserIdentifier provider;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar registrationDate;
    protected SpecialityAccess specialtyAccess;
    protected SimpleIdentifier systemId;

    /**
     * Gets the value of the access property.
     * 
     * @return
     *     possible object is
     *     {@link AccessRights }
     *     
     */
    public AccessRights getAccess() {
        return access;
    }

    /**
     * Sets the value of the access property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessRights }
     *     
     */
    public void setAccess(AccessRights value) {
        this.access = value;
    }

    /**
     * Gets the value of the default property.
     * 
     */
    public boolean isDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     * 
     */
    public void setDefault(boolean value) {
        this._default = value;
    }

    /**
     * Gets the value of the emptyRegistrationDate property.
     * 
     */
    public boolean isEmptyRegistrationDate() {
        return emptyRegistrationDate;
    }

    /**
     * Sets the value of the emptyRegistrationDate property.
     * 
     */
    public void setEmptyRegistrationDate(boolean value) {
        this.emptyRegistrationDate = value;
    }

    /**
     * Gets the value of the facilityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacilityID() {
        return facilityID;
    }

    /**
     * Sets the value of the facilityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilityID(String value) {
        this.facilityID = value;
    }

    /**
     * Gets the value of the organizationID property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationID() {
        return organizationID;
    }

    /**
     * Sets the value of the organizationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationID(OrganizationIdentifier value) {
        this.organizationID = value;
    }

    /**
     * Gets the value of the otherProvider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherProvider() {
        return otherProvider;
    }

    /**
     * Sets the value of the otherProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherProvider(String value) {
        this.otherProvider = value;
    }

    /**
     * Gets the value of the provider property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getProvider() {
        return provider;
    }

    /**
     * Sets the value of the provider property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setProvider(UserIdentifier value) {
        this.provider = value;
    }

    /**
     * Gets the value of the registrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets the value of the registrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegistrationDate(XMLGregorianCalendar value) {
        this.registrationDate = value;
    }

    /**
     * Gets the value of the specialtyAccess property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialityAccess }
     *     
     */
    public SpecialityAccess getSpecialtyAccess() {
        return specialtyAccess;
    }

    /**
     * Sets the value of the specialtyAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialityAccess }
     *     
     */
    public void setSpecialtyAccess(SpecialityAccess value) {
        this.specialtyAccess = value;
    }

    /**
     * Gets the value of the systemId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleIdentifier }
     *     
     */
    public SimpleIdentifier getSystemId() {
        return systemId;
    }

    /**
     * Sets the value of the systemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleIdentifier }
     *     
     */
    public void setSystemId(SimpleIdentifier value) {
        this.systemId = value;
    }

}
