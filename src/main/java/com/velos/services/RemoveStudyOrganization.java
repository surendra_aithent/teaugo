
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeStudyOrganization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeStudyOrganization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="OrganizationIdentifier" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeStudyOrganization", propOrder = {
    "studyIdentifier",
    "organizationIdentifier"
})
public class RemoveStudyOrganization {

    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;
    @XmlElement(name = "OrganizationIdentifier")
    protected OrganizationIdentifier organizationIdentifier;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the organizationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    /**
     * Sets the value of the organizationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationIdentifier(OrganizationIdentifier value) {
        this.organizationIdentifier = value;
    }

}
