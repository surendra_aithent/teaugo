
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateEventCost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateEventCost">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventCostIdentifier" type="{http://velos.com/services/}eventCostIdentifier" minOccurs="0"/>
 *         &lt;element name="Cost" type="{http://velos.com/services/}cost" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateEventCost", propOrder = {
    "eventCostIdentifier",
    "cost"
})
public class UpdateEventCost {

    @XmlElement(name = "EventCostIdentifier")
    protected EventCostIdentifier eventCostIdentifier;
    @XmlElement(name = "Cost")
    protected Cost cost;

    /**
     * Gets the value of the eventCostIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventCostIdentifier }
     *     
     */
    public EventCostIdentifier getEventCostIdentifier() {
        return eventCostIdentifier;
    }

    /**
     * Sets the value of the eventCostIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCostIdentifier }
     *     
     */
    public void setEventCostIdentifier(EventCostIdentifier value) {
        this.eventCostIdentifier = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link Cost }
     *     
     */
    public Cost getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cost }
     *     
     */
    public void setCost(Cost value) {
        this.cost = value;
    }

}
