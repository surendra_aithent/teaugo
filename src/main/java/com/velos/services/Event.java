
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for event complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="event">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="CPTCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="costs" type="{http://velos.com/services/}cost" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coverageType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventCRFs" type="{http://velos.com/services/}eventCRFs" minOccurs="0"/>
 *         &lt;element name="eventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventScheduledDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventStatus" type="{http://velos.com/services/}eventStatus" minOccurs="0"/>
 *         &lt;element name="eventSuggestedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventWindow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventWindowAfter" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="eventWindowBefore" type="{http://velos.com/services/}duration" minOccurs="0"/>
 *         &lt;element name="facility" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scheduleEventIdentifier" type="{http://velos.com/services/}scheduleEventIdentifier" minOccurs="0"/>
 *         &lt;element name="sequence" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="siteOfService" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "event", propOrder = {
    "cptCode",
    "costs",
    "coverageType",
    "description",
    "eventCRFs",
    "eventName",
    "eventScheduledDate",
    "eventStatus",
    "eventSuggestedDate",
    "eventWindow",
    "eventWindowAfter",
    "eventWindowBefore",
    "facility",
    "notes",
    "scheduleEventIdentifier",
    "sequence",
    "siteOfService"
})
public class Event
    extends ServiceObject
{

    @XmlElement(name = "CPTCode")
    protected String cptCode;
    @XmlElement(nillable = true)
    protected List<Cost> costs;
    protected Code coverageType;
    protected String description;
    protected EventCRFs eventCRFs;
    protected String eventName;
    protected String eventScheduledDate;
    protected EventStatus eventStatus;
    protected String eventSuggestedDate;
    protected String eventWindow;
    protected Duration eventWindowAfter;
    protected Duration eventWindowBefore;
    protected OrganizationIdentifier facility;
    protected String notes;
    protected ScheduleEventIdentifier scheduleEventIdentifier;
    protected Integer sequence;
    protected OrganizationIdentifier siteOfService;

    /**
     * Gets the value of the cptCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPTCode() {
        return cptCode;
    }

    /**
     * Sets the value of the cptCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPTCode(String value) {
        this.cptCode = value;
    }

    /**
     * Gets the value of the costs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCosts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cost }
     * 
     * 
     */
    public List<Cost> getCosts() {
        if (costs == null) {
            costs = new ArrayList<Cost>();
        }
        return this.costs;
    }

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCoverageType(Code value) {
        this.coverageType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the eventCRFs property.
     * 
     * @return
     *     possible object is
     *     {@link EventCRFs }
     *     
     */
    public EventCRFs getEventCRFs() {
        return eventCRFs;
    }

    /**
     * Sets the value of the eventCRFs property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCRFs }
     *     
     */
    public void setEventCRFs(EventCRFs value) {
        this.eventCRFs = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventScheduledDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventScheduledDate() {
        return eventScheduledDate;
    }

    /**
     * Sets the value of the eventScheduledDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventScheduledDate(String value) {
        this.eventScheduledDate = value;
    }

    /**
     * Gets the value of the eventStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EventStatus }
     *     
     */
    public EventStatus getEventStatus() {
        return eventStatus;
    }

    /**
     * Sets the value of the eventStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventStatus }
     *     
     */
    public void setEventStatus(EventStatus value) {
        this.eventStatus = value;
    }

    /**
     * Gets the value of the eventSuggestedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventSuggestedDate() {
        return eventSuggestedDate;
    }

    /**
     * Sets the value of the eventSuggestedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventSuggestedDate(String value) {
        this.eventSuggestedDate = value;
    }

    /**
     * Gets the value of the eventWindow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventWindow() {
        return eventWindow;
    }

    /**
     * Sets the value of the eventWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventWindow(String value) {
        this.eventWindow = value;
    }

    /**
     * Gets the value of the eventWindowAfter property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEventWindowAfter() {
        return eventWindowAfter;
    }

    /**
     * Sets the value of the eventWindowAfter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEventWindowAfter(Duration value) {
        this.eventWindowAfter = value;
    }

    /**
     * Gets the value of the eventWindowBefore property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEventWindowBefore() {
        return eventWindowBefore;
    }

    /**
     * Sets the value of the eventWindowBefore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEventWindowBefore(Duration value) {
        this.eventWindowBefore = value;
    }

    /**
     * Gets the value of the facility property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getFacility() {
        return facility;
    }

    /**
     * Sets the value of the facility property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setFacility(OrganizationIdentifier value) {
        this.facility = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the scheduleEventIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleEventIdentifier }
     *     
     */
    public ScheduleEventIdentifier getScheduleEventIdentifier() {
        return scheduleEventIdentifier;
    }

    /**
     * Sets the value of the scheduleEventIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleEventIdentifier }
     *     
     */
    public void setScheduleEventIdentifier(ScheduleEventIdentifier value) {
        this.scheduleEventIdentifier = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequence(Integer value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the siteOfService property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getSiteOfService() {
        return siteOfService;
    }

    /**
     * Sets the value of the siteOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setSiteOfService(OrganizationIdentifier value) {
        this.siteOfService = value;
    }

}
