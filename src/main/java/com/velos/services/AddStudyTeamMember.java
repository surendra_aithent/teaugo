
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addStudyTeamMember complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addStudyTeamMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyTeamMember" type="{http://velos.com/services/}studyTeamMember" minOccurs="0"/>
 *         &lt;element name="createNonSystemUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addStudyTeamMember", propOrder = {
    "studyIdentifier",
    "studyTeamMember",
    "createNonSystemUsers"
})
public class AddStudyTeamMember {

    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;
    @XmlElement(name = "StudyTeamMember")
    protected StudyTeamMember studyTeamMember;
    protected boolean createNonSystemUsers;

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the studyTeamMember property.
     * 
     * @return
     *     possible object is
     *     {@link StudyTeamMember }
     *     
     */
    public StudyTeamMember getStudyTeamMember() {
        return studyTeamMember;
    }

    /**
     * Sets the value of the studyTeamMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyTeamMember }
     *     
     */
    public void setStudyTeamMember(StudyTeamMember value) {
        this.studyTeamMember = value;
    }

    /**
     * Gets the value of the createNonSystemUsers property.
     * 
     */
    public boolean isCreateNonSystemUsers() {
        return createNonSystemUsers;
    }

    /**
     * Sets the value of the createNonSystemUsers property.
     * 
     */
    public void setCreateNonSystemUsers(boolean value) {
        this.createNonSystemUsers = value;
    }

}
