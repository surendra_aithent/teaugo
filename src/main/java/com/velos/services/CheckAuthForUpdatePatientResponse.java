
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkAuthForUpdatePatientResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkAuthForUpdatePatientResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseHolder" type="{http://velos.com/services/}responseHolder" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkAuthForUpdatePatientResponse", propOrder = {
    "responseHolder"
})
public class CheckAuthForUpdatePatientResponse {

    @XmlElement(name = "ResponseHolder")
    protected ResponseHolder responseHolder;

    /**
     * Gets the value of the responseHolder property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHolder }
     *     
     */
    public ResponseHolder getResponseHolder() {
        return responseHolder;
    }

    /**
     * Sets the value of the responseHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHolder }
     *     
     */
    public void setResponseHolder(ResponseHolder value) {
        this.responseHolder = value;
    }

}
