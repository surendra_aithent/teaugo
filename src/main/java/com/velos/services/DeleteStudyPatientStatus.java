
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteStudyPatientStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteStudyPatientStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientStudyStatusIdentifier" type="{http://velos.com/services/}patientStudyStatusIdentifier" minOccurs="0"/>
 *         &lt;element name="reasonForDelete" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteStudyPatientStatus", propOrder = {
    "patientStudyStatusIdentifier",
    "reasonForDelete"
})
public class DeleteStudyPatientStatus {

    @XmlElement(name = "PatientStudyStatusIdentifier")
    protected PatientStudyStatusIdentifier patientStudyStatusIdentifier;
    protected String reasonForDelete;

    /**
     * Gets the value of the patientStudyStatusIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public PatientStudyStatusIdentifier getPatientStudyStatusIdentifier() {
        return patientStudyStatusIdentifier;
    }

    /**
     * Sets the value of the patientStudyStatusIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientStudyStatusIdentifier }
     *     
     */
    public void setPatientStudyStatusIdentifier(PatientStudyStatusIdentifier value) {
        this.patientStudyStatusIdentifier = value;
    }

    /**
     * Gets the value of the reasonForDelete property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForDelete() {
        return reasonForDelete;
    }

    /**
     * Sets the value of the reasonForDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForDelete(String value) {
        this.reasonForDelete = value;
    }

}
