
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for organizationIdentifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="organizationIdentifier">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}simpleIdentifier">
 *       &lt;sequence>
 *         &lt;element name="siteAltId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "organizationIdentifier", propOrder = {
    "siteAltId",
    "siteName"
})
public class OrganizationIdentifier
    extends SimpleIdentifier
{

    protected String siteAltId;
    protected String siteName;

    /**
     * Gets the value of the siteAltId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteAltId() {
        return siteAltId;
    }

    /**
     * Sets the value of the siteAltId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteAltId(String value) {
        this.siteAltId = value;
    }

    /**
     * Gets the value of the siteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteName(String value) {
        this.siteName = value;
    }

}
