
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for patientSchedules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="patientSchedules">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="patientSchedule" type="{http://velos.com/services/}patientScheduleSummary" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "patientSchedules", propOrder = {
    "patientIdentifier",
    "patientSchedule",
    "studyIdentifier"
})
public class PatientSchedules
    extends ServiceObject
{

    protected PatientIdentifier patientIdentifier;
    @XmlElement(nillable = true)
    protected List<PatientScheduleSummary> patientSchedule;
    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the patientSchedule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the patientSchedule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPatientSchedule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PatientScheduleSummary }
     * 
     * 
     */
    public List<PatientScheduleSummary> getPatientSchedule() {
        if (patientSchedule == null) {
            patientSchedule = new ArrayList<PatientScheduleSummary>();
        }
        return this.patientSchedule;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
