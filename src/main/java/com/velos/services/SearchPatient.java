
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchPatient complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchPatient">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientSearch" type="{http://velos.com/services/}patientSearch" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchPatient", propOrder = {
    "patientSearch"
})
public class SearchPatient {

    @XmlElement(name = "PatientSearch")
    protected PatientSearch patientSearch;

    /**
     * Gets the value of the patientSearch property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSearch }
     *     
     */
    public PatientSearch getPatientSearch() {
        return patientSearch;
    }

    /**
     * Sets the value of the patientSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSearch }
     *     
     */
    public void setPatientSearch(PatientSearch value) {
        this.patientSearch = value;
    }

}
