
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createMEventStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createMEventStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MEventStatuses" type="{http://velos.com/services/}mEventStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMEventStatus", propOrder = {
    "mEventStatuses"
})
public class CreateMEventStatus {

    @XmlElement(name = "MEventStatuses")
    protected MEventStatuses mEventStatuses;

    /**
     * Gets the value of the mEventStatuses property.
     * 
     * @return
     *     possible object is
     *     {@link MEventStatuses }
     *     
     */
    public MEventStatuses getMEventStatuses() {
        return mEventStatuses;
    }

    /**
     * Sets the value of the mEventStatuses property.
     * 
     * @param value
     *     allowed object is
     *     {@link MEventStatuses }
     *     
     */
    public void setMEventStatuses(MEventStatuses value) {
        this.mEventStatuses = value;
    }

}
