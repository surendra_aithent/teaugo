
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarVisits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarVisits">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="visit" type="{http://velos.com/services/}calendarVisit" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarVisits", propOrder = {
    "visit"
})
public class CalendarVisits
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<CalendarVisit> visit;

    /**
     * Gets the value of the visit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the visit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVisit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalendarVisit }
     * 
     * 
     */
    public List<CalendarVisit> getVisit() {
        if (visit == null) {
            visit = new ArrayList<CalendarVisit>();
        }
        return this.visit;
    }

}
