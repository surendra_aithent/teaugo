
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userSearchOrderBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="userSearchOrderBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="firstName"/>
 *     &lt;enumeration value="lastName"/>
 *     &lt;enumeration value="email"/>
 *     &lt;enumeration value="siteName"/>
 *     &lt;enumeration value="userStatus"/>
 *     &lt;enumeration value="userLoginName"/>
 *     &lt;enumeration value="PK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "userSearchOrderBy")
@XmlEnum
public enum UserSearchOrderBy {

    @XmlEnumValue("firstName")
    FIRST_NAME("firstName"),
    @XmlEnumValue("lastName")
    LAST_NAME("lastName"),
    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("siteName")
    SITE_NAME("siteName"),
    @XmlEnumValue("userStatus")
    USER_STATUS("userStatus"),
    @XmlEnumValue("userLoginName")
    USER_LOGIN_NAME("userLoginName"),
    PK("PK");
    private final String value;

    UserSearchOrderBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UserSearchOrderBy fromValue(String v) {
        for (UserSearchOrderBy c: UserSearchOrderBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
