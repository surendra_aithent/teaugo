
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBudgetStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBudgetStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BudgetStatus" type="{http://velos.com/services/}budgetStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBudgetStatusResponse", propOrder = {
    "budgetStatus"
})
public class GetBudgetStatusResponse {

    @XmlElement(name = "BudgetStatus")
    protected BudgetStatus budgetStatus;

    /**
     * Gets the value of the budgetStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetStatus }
     *     
     */
    public BudgetStatus getBudgetStatus() {
        return budgetStatus;
    }

    /**
     * Sets the value of the budgetStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetStatus }
     *     
     */
    public void setBudgetStatus(BudgetStatus value) {
        this.budgetStatus = value;
    }

}
