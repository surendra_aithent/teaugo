
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyMashupResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyMashupResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyMashup" type="{http://velos.com/services/}studyMashup" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyMashupResponse", propOrder = {
    "studyMashup"
})
public class GetStudyMashupResponse {

    @XmlElement(name = "StudyMashup")
    protected StudyMashup studyMashup;

    /**
     * Gets the value of the studyMashup property.
     * 
     * @return
     *     possible object is
     *     {@link StudyMashup }
     *     
     */
    public StudyMashup getStudyMashup() {
        return studyMashup;
    }

    /**
     * Sets the value of the studyMashup property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyMashup }
     *     
     */
    public void setStudyMashup(StudyMashup value) {
        this.studyMashup = value;
    }

}
