
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePatientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePatientFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormResponseIdentifier" type="{http://velos.com/services/}patientFormResponseIdentifier" minOccurs="0"/>
 *         &lt;element name="PatientFormResponse" type="{http://velos.com/services/}patientFormResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePatientFormResponse", propOrder = {
    "formResponseIdentifier",
    "patientFormResponse"
})
public class UpdatePatientFormResponse {

    @XmlElement(name = "FormResponseIdentifier")
    protected PatientFormResponseIdentifier formResponseIdentifier;
    @XmlElement(name = "PatientFormResponse")
    protected PatientFormResponse patientFormResponse;

    /**
     * Gets the value of the formResponseIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public PatientFormResponseIdentifier getFormResponseIdentifier() {
        return formResponseIdentifier;
    }

    /**
     * Sets the value of the formResponseIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientFormResponseIdentifier }
     *     
     */
    public void setFormResponseIdentifier(PatientFormResponseIdentifier value) {
        this.formResponseIdentifier = value;
    }

    /**
     * Gets the value of the patientFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link PatientFormResponse }
     *     
     */
    public PatientFormResponse getPatientFormResponse() {
        return patientFormResponse;
    }

    /**
     * Sets the value of the patientFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientFormResponse }
     *     
     */
    public void setPatientFormResponse(PatientFormResponse value) {
        this.patientFormResponse = value;
    }

}
