
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for scheduleEventStatuses complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleEventStatuses">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="scheduleEventStatInfo" type="{http://velos.com/services/}scheduleEventStatusIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleEventStatuses", propOrder = {
    "scheduleEventStatInfo"
})
public class ScheduleEventStatuses
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<ScheduleEventStatusIdentifier> scheduleEventStatInfo;

    /**
     * Gets the value of the scheduleEventStatInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scheduleEventStatInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleEventStatInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduleEventStatusIdentifier }
     * 
     * 
     */
    public List<ScheduleEventStatusIdentifier> getScheduleEventStatInfo() {
        if (scheduleEventStatInfo == null) {
            scheduleEventStatInfo = new ArrayList<ScheduleEventStatusIdentifier>();
        }
        return this.scheduleEventStatInfo;
    }

}
