
package com.velos.services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getPatientStudiesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getPatientStudiesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientStudy" type="{http://velos.com/services/}patientStudy" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPatientStudiesResponse", propOrder = {
    "patientStudy"
})
@XmlRootElement(name="getPatientStudiesResponse", namespace = "http://velos.com/services/")
public class GetPatientStudiesResponse {

    @XmlElement(name = "PatientStudy")
    protected List<PatientStudy> patientStudy;

    /**
     * Gets the value of the patientStudy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the patientStudy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPatientStudy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PatientStudy }
     * 
     * 
     */
    public List<PatientStudy> getPatientStudy() {
        if (patientStudy == null) {
            patientStudy = new ArrayList<PatientStudy>();
        }
        return this.patientStudy;
    }

}
