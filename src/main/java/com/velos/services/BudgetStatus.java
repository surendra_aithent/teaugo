
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for budgetStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="budgetStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="budgetIdentifier" type="{http://velos.com/services/}budgetIdentifier" minOccurs="0"/>
 *         &lt;element name="budgetStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="statusChangedBy" type="{http://velos.com/services/}userIdentifier" minOccurs="0"/>
 *         &lt;element name="statusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "budgetStatus", propOrder = {
    "budgetIdentifier",
    "budgetStatus",
    "statusChangedBy",
    "statusDate"
})
public class BudgetStatus
    extends ServiceObject
{

    protected BudgetIdentifier budgetIdentifier;
    protected Code budgetStatus;
    protected UserIdentifier statusChangedBy;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusDate;

    /**
     * Gets the value of the budgetIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetIdentifier }
     *     
     */
    public BudgetIdentifier getBudgetIdentifier() {
        return budgetIdentifier;
    }

    /**
     * Sets the value of the budgetIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetIdentifier }
     *     
     */
    public void setBudgetIdentifier(BudgetIdentifier value) {
        this.budgetIdentifier = value;
    }

    /**
     * Gets the value of the budgetStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getBudgetStatus() {
        return budgetStatus;
    }

    /**
     * Sets the value of the budgetStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setBudgetStatus(Code value) {
        this.budgetStatus = value;
    }

    /**
     * Gets the value of the statusChangedBy property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifier }
     *     
     */
    public UserIdentifier getStatusChangedBy() {
        return statusChangedBy;
    }

    /**
     * Sets the value of the statusChangedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifier }
     *     
     */
    public void setStatusChangedBy(UserIdentifier value) {
        this.statusChangedBy = value;
    }

    /**
     * Gets the value of the statusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusDate() {
        return statusDate;
    }

    /**
     * Sets the value of the statusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusDate(XMLGregorianCalendar value) {
        this.statusDate = value;
    }

}
