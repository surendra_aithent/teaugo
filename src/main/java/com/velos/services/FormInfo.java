
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="dataCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="formDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formIdentifier" type="{http://velos.com/services/}formIdentifier" minOccurs="0"/>
 *         &lt;element name="formName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formStatus" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formInfo", propOrder = {
    "dataCount",
    "formDesc",
    "formIdentifier",
    "formName",
    "formStatus"
})
public class FormInfo
    extends ServiceObject
{

    protected Integer dataCount;
    protected String formDesc;
    protected FormIdentifier formIdentifier;
    protected String formName;
    protected Code formStatus;

    /**
     * Gets the value of the dataCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataCount() {
        return dataCount;
    }

    /**
     * Sets the value of the dataCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataCount(Integer value) {
        this.dataCount = value;
    }

    /**
     * Gets the value of the formDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormDesc() {
        return formDesc;
    }

    /**
     * Sets the value of the formDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormDesc(String value) {
        this.formDesc = value;
    }

    /**
     * Gets the value of the formIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FormIdentifier }
     *     
     */
    public FormIdentifier getFormIdentifier() {
        return formIdentifier;
    }

    /**
     * Sets the value of the formIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormIdentifier }
     *     
     */
    public void setFormIdentifier(FormIdentifier value) {
        this.formIdentifier = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

    /**
     * Gets the value of the formStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getFormStatus() {
        return formStatus;
    }

    /**
     * Sets the value of the formStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setFormStatus(Code value) {
        this.formStatus = value;
    }

}
