
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for studyOrganizations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="studyOrganizations">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="parentIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="studyOrganizaton" type="{http://velos.com/services/}studyOrganization" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "studyOrganizations", propOrder = {
    "parentIdentifier",
    "studyOrganizaton"
})
public class StudyOrganizations
    extends ServiceObject
{

    protected StudyIdentifier parentIdentifier;
    @XmlElement(nillable = true)
    protected List<StudyOrganization> studyOrganizaton;

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setParentIdentifier(StudyIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the studyOrganizaton property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studyOrganizaton property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudyOrganizaton().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StudyOrganization }
     * 
     * 
     */
    public List<StudyOrganization> getStudyOrganizaton() {
        if (studyOrganizaton == null) {
            studyOrganizaton = new ArrayList<StudyOrganization>();
        }
        return this.studyOrganizaton;
    }

}
