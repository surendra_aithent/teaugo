
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyStatus" type="{http://velos.com/services/}studyStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyStatusResponse", propOrder = {
    "studyStatus"
})
public class GetStudyStatusResponse {

    @XmlElement(name = "StudyStatus")
    protected StudyStatus studyStatus;

    /**
     * Gets the value of the studyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StudyStatus }
     *     
     */
    public StudyStatus getStudyStatus() {
        return studyStatus;
    }

    /**
     * Sets the value of the studyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyStatus }
     *     
     */
    public void setStudyStatus(StudyStatus value) {
        this.studyStatus = value;
    }

}
