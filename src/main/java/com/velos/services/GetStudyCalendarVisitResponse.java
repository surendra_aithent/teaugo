
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalendarVisitResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalendarVisitResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Visit" type="{http://velos.com/services/}calendarVisit" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalendarVisitResponse", propOrder = {
    "visit"
})
@XmlRootElement(name="getStudyCalendarVisitResponse", namespace = "http://velos.com/services/")
public class GetStudyCalendarVisitResponse {

    @XmlElement(name = "Visit")
    protected CalendarVisit visit;

    /**
     * Gets the value of the visit property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarVisit }
     *     
     */
    public CalendarVisit getVisit() {
        return visit;
    }

    /**
     * Sets the value of the visit property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarVisit }
     *     
     */
    public void setVisit(CalendarVisit value) {
        this.visit = value;
    }

}
