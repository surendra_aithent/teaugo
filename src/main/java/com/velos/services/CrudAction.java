
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for crudAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="crudAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CREATE"/>
 *     &lt;enumeration value="RETRIEVE"/>
 *     &lt;enumeration value="UPDATE"/>
 *     &lt;enumeration value="REMOVE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "crudAction")
@XmlEnum
public enum CrudAction {

    CREATE,
    RETRIEVE,
    UPDATE,
    REMOVE;

    public String value() {
        return name();
    }

    public static CrudAction fromValue(String v) {
        return valueOf(v);
    }

}
