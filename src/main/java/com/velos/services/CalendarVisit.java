
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarVisit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarVisit">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="calendarVisitSummary" type="{http://velos.com/services/}calendarVisitSummary" minOccurs="0"/>
 *         &lt;element name="events" type="{http://velos.com/services/}calendarEvents" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarVisit", propOrder = {
    "calendarVisitSummary",
    "events"
})
public class CalendarVisit
    extends ServiceObject
{

    protected CalendarVisitSummary calendarVisitSummary;
    protected CalendarEvents events;

    /**
     * Gets the value of the calendarVisitSummary property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarVisitSummary }
     *     
     */
    public CalendarVisitSummary getCalendarVisitSummary() {
        return calendarVisitSummary;
    }

    /**
     * Sets the value of the calendarVisitSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarVisitSummary }
     *     
     */
    public void setCalendarVisitSummary(CalendarVisitSummary value) {
        this.calendarVisitSummary = value;
    }

    /**
     * Gets the value of the events property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarEvents }
     *     
     */
    public CalendarEvents getEvents() {
        return events;
    }

    /**
     * Sets the value of the events property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarEvents }
     *     
     */
    public void setEvents(CalendarEvents value) {
        this.events = value;
    }

}
