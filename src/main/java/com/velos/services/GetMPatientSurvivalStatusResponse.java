
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMPatientSurvivalStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMPatientSurvivalStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getMPatientSurvivalStatus" type="{http://velos.com/services/}patientSurvivalStatuses" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPatientSurvivalStatusResponse", propOrder = {
    "getMPatientSurvivalStatus"
})
public class GetMPatientSurvivalStatusResponse {

    protected PatientSurvivalStatuses getMPatientSurvivalStatus;

    /**
     * Gets the value of the getMPatientSurvivalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link PatientSurvivalStatuses }
     *     
     */
    public PatientSurvivalStatuses getGetMPatientSurvivalStatus() {
        return getMPatientSurvivalStatus;
    }

    /**
     * Sets the value of the getMPatientSurvivalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientSurvivalStatuses }
     *     
     */
    public void setGetMPatientSurvivalStatus(PatientSurvivalStatuses value) {
        this.getMPatientSurvivalStatus = value;
    }

}
