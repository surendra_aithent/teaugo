
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyCalendarEventResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyCalendarEventResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Event" type="{http://velos.com/services/}calendarEvent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyCalendarEventResponse", propOrder = {
    "event"
})
@XmlRootElement(name="getStudyCalendarEventResponse", namespace = "http://velos.com/services/")
public class GetStudyCalendarEventResponse {

    @XmlElement(name = "Event")
    protected CalendarEvent event;

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarEvent }
     *     
     */
    public CalendarEvent getEvent() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarEvent }
     *     
     */
    public void setEvent(CalendarEvent value) {
        this.event = value;
    }

}
