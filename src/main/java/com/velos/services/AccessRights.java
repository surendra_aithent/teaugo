
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accessRights.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="accessRights">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GRANTED"/>
 *     &lt;enumeration value="REVOKED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "accessRights")
@XmlEnum
public enum AccessRights {

    GRANTED,
    REVOKED;

    public String value() {
        return name();
    }

    public static AccessRights fromValue(String v) {
        return valueOf(v);
    }

}
