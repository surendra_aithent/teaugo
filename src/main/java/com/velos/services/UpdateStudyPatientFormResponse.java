
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateStudyPatientFormResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateStudyPatientFormResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormResponseIdentifier" type="{http://velos.com/services/}studyPatientFormResponseIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyPatientFormResponse" type="{http://velos.com/services/}formResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateStudyPatientFormResponse", propOrder = {
    "formResponseIdentifier",
    "studyPatientFormResponse"
})
public class UpdateStudyPatientFormResponse {

    @XmlElement(name = "FormResponseIdentifier")
    protected StudyPatientFormResponseIdentifier formResponseIdentifier;
    @XmlElement(name = "StudyPatientFormResponse")
    protected FormResponse studyPatientFormResponse;

    /**
     * Gets the value of the formResponseIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientFormResponseIdentifier }
     *     
     */
    public StudyPatientFormResponseIdentifier getFormResponseIdentifier() {
        return formResponseIdentifier;
    }

    /**
     * Sets the value of the formResponseIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientFormResponseIdentifier }
     *     
     */
    public void setFormResponseIdentifier(StudyPatientFormResponseIdentifier value) {
        this.formResponseIdentifier = value;
    }

    /**
     * Gets the value of the studyPatientFormResponse property.
     * 
     * @return
     *     possible object is
     *     {@link FormResponse }
     *     
     */
    public FormResponse getStudyPatientFormResponse() {
        return studyPatientFormResponse;
    }

    /**
     * Sets the value of the studyPatientFormResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormResponse }
     *     
     */
    public void setStudyPatientFormResponse(FormResponse value) {
        this.studyPatientFormResponse = value;
    }

}
