
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePatientFacility complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePatientFacility">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="OrganizationIdentifier" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="PatientOrganizationIdentifier" type="{http://velos.com/services/}patientOrganizationIdentifier" minOccurs="0"/>
 *         &lt;element name="PatientOrganization" type="{http://velos.com/services/}patientOrganization" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePatientFacility", propOrder = {
    "patientIdentifier",
    "organizationIdentifier",
    "patientOrganizationIdentifier",
    "patientOrganization"
})
public class UpdatePatientFacility {

    @XmlElement(name = "PatientIdentifier")
    protected PatientIdentifier patientIdentifier;
    @XmlElement(name = "OrganizationIdentifier")
    protected OrganizationIdentifier organizationIdentifier;
    @XmlElement(name = "PatientOrganizationIdentifier")
    protected PatientOrganizationIdentifier patientOrganizationIdentifier;
    @XmlElement(name = "PatientOrganization")
    protected PatientOrganization patientOrganization;

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the organizationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getOrganizationIdentifier() {
        return organizationIdentifier;
    }

    /**
     * Sets the value of the organizationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setOrganizationIdentifier(OrganizationIdentifier value) {
        this.organizationIdentifier = value;
    }

    /**
     * Gets the value of the patientOrganizationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientOrganizationIdentifier }
     *     
     */
    public PatientOrganizationIdentifier getPatientOrganizationIdentifier() {
        return patientOrganizationIdentifier;
    }

    /**
     * Sets the value of the patientOrganizationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientOrganizationIdentifier }
     *     
     */
    public void setPatientOrganizationIdentifier(PatientOrganizationIdentifier value) {
        this.patientOrganizationIdentifier = value;
    }

    /**
     * Gets the value of the patientOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link PatientOrganization }
     *     
     */
    public PatientOrganization getPatientOrganization() {
        return patientOrganization;
    }

    /**
     * Sets the value of the patientOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientOrganization }
     *     
     */
    public void setPatientOrganization(PatientOrganization value) {
        this.patientOrganization = value;
    }

}
