
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addUnscheduledAdminEvents complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addUnscheduledAdminEvents">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScheduleIdentifier" type="{http://velos.com/services/}calendarIdentifier" minOccurs="0"/>
 *         &lt;element name="Events" type="{http://velos.com/services/}multipleEvents" minOccurs="0"/>
 *         &lt;element name="VisitIdentifier" type="{http://velos.com/services/}visitIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addUnscheduledAdminEvents", propOrder = {
    "scheduleIdentifier",
    "events",
    "visitIdentifier"
})
public class AddUnscheduledAdminEvents {

    @XmlElement(name = "ScheduleIdentifier")
    protected CalendarIdentifier scheduleIdentifier;
    @XmlElement(name = "Events")
    protected MultipleEvents events;
    @XmlElement(name = "VisitIdentifier")
    protected VisitIdentifier visitIdentifier;

    /**
     * Gets the value of the scheduleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CalendarIdentifier }
     *     
     */
    public CalendarIdentifier getScheduleIdentifier() {
        return scheduleIdentifier;
    }

    /**
     * Sets the value of the scheduleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalendarIdentifier }
     *     
     */
    public void setScheduleIdentifier(CalendarIdentifier value) {
        this.scheduleIdentifier = value;
    }

    /**
     * Gets the value of the events property.
     * 
     * @return
     *     possible object is
     *     {@link MultipleEvents }
     *     
     */
    public MultipleEvents getEvents() {
        return events;
    }

    /**
     * Sets the value of the events property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultipleEvents }
     *     
     */
    public void setEvents(MultipleEvents value) {
        this.events = value;
    }

    /**
     * Gets the value of the visitIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link VisitIdentifier }
     *     
     */
    public VisitIdentifier getVisitIdentifier() {
        return visitIdentifier;
    }

    /**
     * Sets the value of the visitIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisitIdentifier }
     *     
     */
    public void setVisitIdentifier(VisitIdentifier value) {
        this.visitIdentifier = value;
    }

}
