
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkedFormDesign complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkedFormDesign">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}formDesign">
 *       &lt;sequence>
 *         &lt;element name="dataCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="displayType" type="{http://velos.com/services/}displayType" minOccurs="0"/>
 *         &lt;element name="entrySetting" type="{http://velos.com/services/}entrySettings" minOccurs="0"/>
 *         &lt;element name="IRBForm" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="submissionType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkedFormDesign", propOrder = {
    "dataCount",
    "displayType",
    "entrySetting",
    "irbForm",
    "submissionType"
})
@XmlSeeAlso({
    StudyFormDesign.class
})
public class LinkedFormDesign
    extends FormDesign
{

    protected Integer dataCount;
    protected DisplayType displayType;
    protected EntrySettings entrySetting;
    @XmlElement(name = "IRBForm")
    protected boolean irbForm;
    protected Code submissionType;

    /**
     * Gets the value of the dataCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataCount() {
        return dataCount;
    }

    /**
     * Sets the value of the dataCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataCount(Integer value) {
        this.dataCount = value;
    }

    /**
     * Gets the value of the displayType property.
     * 
     * @return
     *     possible object is
     *     {@link DisplayType }
     *     
     */
    public DisplayType getDisplayType() {
        return displayType;
    }

    /**
     * Sets the value of the displayType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisplayType }
     *     
     */
    public void setDisplayType(DisplayType value) {
        this.displayType = value;
    }

    /**
     * Gets the value of the entrySetting property.
     * 
     * @return
     *     possible object is
     *     {@link EntrySettings }
     *     
     */
    public EntrySettings getEntrySetting() {
        return entrySetting;
    }

    /**
     * Sets the value of the entrySetting property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntrySettings }
     *     
     */
    public void setEntrySetting(EntrySettings value) {
        this.entrySetting = value;
    }

    /**
     * Gets the value of the irbForm property.
     * 
     */
    public boolean isIRBForm() {
        return irbForm;
    }

    /**
     * Sets the value of the irbForm property.
     * 
     */
    public void setIRBForm(boolean value) {
        this.irbForm = value;
    }

    /**
     * Gets the value of the submissionType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getSubmissionType() {
        return submissionType;
    }

    /**
     * Sets the value of the submissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setSubmissionType(Code value) {
        this.submissionType = value;
    }

}
