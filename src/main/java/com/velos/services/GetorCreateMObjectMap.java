
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getorCreateMObjectMap complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getorCreateMObjectMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TableInfos" type="{http://velos.com/services/}objectInfos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getorCreateMObjectMap", propOrder = {
    "tableInfos"
})
public class GetorCreateMObjectMap {

    @XmlElement(name = "TableInfos")
    protected ObjectInfos tableInfos;

    /**
     * Gets the value of the tableInfos property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectInfos }
     *     
     */
    public ObjectInfos getTableInfos() {
        return tableInfos;
    }

    /**
     * Sets the value of the tableInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectInfos }
     *     
     */
    public void setTableInfos(ObjectInfos value) {
        this.tableInfos = value;
    }

}
