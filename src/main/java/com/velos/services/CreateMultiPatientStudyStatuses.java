
package com.velos.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createMultiPatientStudyStatuses complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createMultiPatientStudyStatuses">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="createMPatientStudyStatusList" type="{http://velos.com/services/}createMultiPatientStudyStatus" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMultiPatientStudyStatuses", propOrder = {
    "createMPatientStudyStatusList"
})
public class CreateMultiPatientStudyStatuses
    extends ServiceObject
{

    @XmlElement(nillable = true)
    protected List<CreateMultiPatientStudyStatus> createMPatientStudyStatusList;

    /**
     * Gets the value of the createMPatientStudyStatusList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createMPatientStudyStatusList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreateMPatientStudyStatusList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateMultiPatientStudyStatus }
     * 
     * 
     */
    public List<CreateMultiPatientStudyStatus> getCreateMPatientStudyStatusList() {
        if (createMPatientStudyStatusList == null) {
            createMPatientStudyStatusList = new ArrayList<CreateMultiPatientStudyStatus>();
        }
        return this.createMPatientStudyStatusList;
    }

}
