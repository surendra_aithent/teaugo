
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createStudy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createStudy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Study" type="{http://velos.com/services/}study" minOccurs="0"/>
 *         &lt;element name="createNonSystemUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createStudy", propOrder = {
    "study",
    "createNonSystemUsers"
})
public class CreateStudy {

    @XmlElement(name = "Study")
    protected Study study;
    protected boolean createNonSystemUsers;

    /**
     * Gets the value of the study property.
     * 
     * @return
     *     possible object is
     *     {@link Study }
     *     
     */
    public Study getStudy() {
        return study;
    }

    /**
     * Sets the value of the study property.
     * 
     * @param value
     *     allowed object is
     *     {@link Study }
     *     
     */
    public void setStudy(Study value) {
        this.study = value;
    }

    /**
     * Gets the value of the createNonSystemUsers property.
     * 
     */
    public boolean isCreateNonSystemUsers() {
        return createNonSystemUsers;
    }

    /**
     * Sets the value of the createNonSystemUsers property.
     * 
     */
    public void setCreateNonSystemUsers(boolean value) {
        this.createNonSystemUsers = value;
    }

}
