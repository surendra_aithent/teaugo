
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyMashupDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyMashupDetailsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StudyMashupDetailsResponse" type="{http://velos.com/services/}studyMashupDetailsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyMashupDetailsResponse", propOrder = {
    "studyMashupDetailsResponse"
})
public class GetStudyMashupDetailsResponse {

    @XmlElement(name = "StudyMashupDetailsResponse")
    protected StudyMashupDetailsResponse studyMashupDetailsResponse;

    /**
     * Gets the value of the studyMashupDetailsResponse property.
     * 
     * @return
     *     possible object is
     *     {@link StudyMashupDetailsResponse }
     *     
     */
    public StudyMashupDetailsResponse getStudyMashupDetailsResponse() {
        return studyMashupDetailsResponse;
    }

    /**
     * Sets the value of the studyMashupDetailsResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyMashupDetailsResponse }
     *     
     */
    public void setStudyMashupDetailsResponse(StudyMashupDetailsResponse value) {
        this.studyMashupDetailsResponse = value;
    }

}
