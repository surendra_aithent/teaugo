
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for numberFieldValidations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="numberFieldValidations">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}textFieldValidations">
 *       &lt;sequence>
 *         &lt;element name="format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="overrideFormat" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="overrideRange" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="range" type="{http://velos.com/services/}numberRange" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "numberFieldValidations", propOrder = {
    "format",
    "overrideFormat",
    "overrideRange",
    "range"
})
public class NumberFieldValidations
    extends TextFieldValidations
{

    protected String format;
    protected boolean overrideFormat;
    protected boolean overrideRange;
    protected NumberRange range;

    /**
     * Gets the value of the format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the value of the format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormat(String value) {
        this.format = value;
    }

    /**
     * Gets the value of the overrideFormat property.
     * 
     */
    public boolean isOverrideFormat() {
        return overrideFormat;
    }

    /**
     * Sets the value of the overrideFormat property.
     * 
     */
    public void setOverrideFormat(boolean value) {
        this.overrideFormat = value;
    }

    /**
     * Gets the value of the overrideRange property.
     * 
     */
    public boolean isOverrideRange() {
        return overrideRange;
    }

    /**
     * Sets the value of the overrideRange property.
     * 
     */
    public void setOverrideRange(boolean value) {
        this.overrideRange = value;
    }

    /**
     * Gets the value of the range property.
     * 
     * @return
     *     possible object is
     *     {@link NumberRange }
     *     
     */
    public NumberRange getRange() {
        return range;
    }

    /**
     * Sets the value of the range property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberRange }
     *     
     */
    public void setRange(NumberRange value) {
        this.range = value;
    }

}
