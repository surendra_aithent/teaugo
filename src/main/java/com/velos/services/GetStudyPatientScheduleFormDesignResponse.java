
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyPatientScheduleFormDesignResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyPatientScheduleFormDesignResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormDesign" type="{http://velos.com/services/}studyPatientScheduleFormDesign" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyPatientScheduleFormDesignResponse", propOrder = {
    "formDesign"
})
public class GetStudyPatientScheduleFormDesignResponse {

    @XmlElement(name = "FormDesign")
    protected StudyPatientScheduleFormDesign formDesign;

    /**
     * Gets the value of the formDesign property.
     * 
     * @return
     *     possible object is
     *     {@link StudyPatientScheduleFormDesign }
     *     
     */
    public StudyPatientScheduleFormDesign getFormDesign() {
        return formDesign;
    }

    /**
     * Sets the value of the formDesign property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyPatientScheduleFormDesign }
     *     
     */
    public void setFormDesign(StudyPatientScheduleFormDesign value) {
        this.formDesign = value;
    }

}
