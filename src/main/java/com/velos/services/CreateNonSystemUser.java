
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createNonSystemUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createNonSystemUser">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NonSystemUser" type="{http://velos.com/services/}nonSystemUser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createNonSystemUser", propOrder = {
    "nonSystemUser"
})
public class CreateNonSystemUser {

    @XmlElement(name = "NonSystemUser")
    protected NonSystemUser nonSystemUser;

    /**
     * Gets the value of the nonSystemUser property.
     * 
     * @return
     *     possible object is
     *     {@link NonSystemUser }
     *     
     */
    public NonSystemUser getNonSystemUser() {
        return nonSystemUser;
    }

    /**
     * Sets the value of the nonSystemUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonSystemUser }
     *     
     */
    public void setNonSystemUser(NonSystemUser value) {
        this.nonSystemUser = value;
    }

}
