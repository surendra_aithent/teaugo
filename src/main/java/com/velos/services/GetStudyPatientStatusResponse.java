
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStudyPatientStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStudyPatientStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientEnrollmentDetails" type="{http://velos.com/services/}patientEnrollmentDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStudyPatientStatusResponse", propOrder = {
    "patientEnrollmentDetails"
})
@XmlRootElement(name="getStudyPatientStatusResponse", namespace = "http://velos.com/services/")
public class GetStudyPatientStatusResponse {

    @XmlElement(name = "PatientEnrollmentDetails")
    protected PatientEnrollmentDetails patientEnrollmentDetails;

    /**
     * Gets the value of the patientEnrollmentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public PatientEnrollmentDetails getPatientEnrollmentDetails() {
        return patientEnrollmentDetails;
    }

    /**
     * Sets the value of the patientEnrollmentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public void setPatientEnrollmentDetails(PatientEnrollmentDetails value) {
        this.patientEnrollmentDetails = value;
    }

}
