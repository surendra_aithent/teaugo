
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBudgetStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBudgetStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BudgetIdentifier" type="{http://velos.com/services/}budgetIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBudgetStatus", propOrder = {
    "budgetIdentifier"
})
public class GetBudgetStatus {

    @XmlElement(name = "BudgetIdentifier")
    protected BudgetIdentifier budgetIdentifier;

    /**
     * Gets the value of the budgetIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BudgetIdentifier }
     *     
     */
    public BudgetIdentifier getBudgetIdentifier() {
        return budgetIdentifier;
    }

    /**
     * Sets the value of the budgetIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BudgetIdentifier }
     *     
     */
    public void setBudgetIdentifier(BudgetIdentifier value) {
        this.budgetIdentifier = value;
    }

}
