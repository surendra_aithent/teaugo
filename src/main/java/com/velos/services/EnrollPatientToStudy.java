
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enrollPatientToStudy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enrollPatientToStudy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="StudyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *         &lt;element name="PatientEnrollmentDetails" type="{http://velos.com/services/}patientEnrollmentDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enrollPatientToStudy", propOrder = {
    "patientIdentifier",
    "studyIdentifier",
    "patientEnrollmentDetails"
})
public class EnrollPatientToStudy {

    @XmlElement(name = "PatientIdentifier")
    protected PatientIdentifier patientIdentifier;
    @XmlElement(name = "StudyIdentifier")
    protected StudyIdentifier studyIdentifier;
    @XmlElement(name = "PatientEnrollmentDetails")
    protected PatientEnrollmentDetails patientEnrollmentDetails;

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

    /**
     * Gets the value of the patientEnrollmentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public PatientEnrollmentDetails getPatientEnrollmentDetails() {
        return patientEnrollmentDetails;
    }

    /**
     * Sets the value of the patientEnrollmentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public void setPatientEnrollmentDetails(PatientEnrollmentDetails value) {
        this.patientEnrollmentDetails = value;
    }

}
