
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sharedWith.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sharedWith">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRIVATE"/>
 *     &lt;enumeration value="USERS"/>
 *     &lt;enumeration value="GROUPS"/>
 *     &lt;enumeration value="STUDYTEAM"/>
 *     &lt;enumeration value="ORGANIZATION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "sharedWith")
@XmlEnum
public enum SharedWith {

    PRIVATE,
    USERS,
    GROUPS,
    STUDYTEAM,
    ORGANIZATION;

    public String value() {
        return name();
    }

    public static SharedWith fromValue(String v) {
        return valueOf(v);
    }

}
