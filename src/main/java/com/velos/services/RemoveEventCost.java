
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeEventCost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeEventCost">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventCostIdentifier" type="{http://velos.com/services/}eventCostIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeEventCost", propOrder = {
    "eventCostIdentifier"
})
public class RemoveEventCost {

    @XmlElement(name = "EventCostIdentifier")
    protected EventCostIdentifier eventCostIdentifier;

    /**
     * Gets the value of the eventCostIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EventCostIdentifier }
     *     
     */
    public EventCostIdentifier getEventCostIdentifier() {
        return eventCostIdentifier;
    }

    /**
     * Sets the value of the eventCostIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventCostIdentifier }
     *     
     */
    public void setEventCostIdentifier(EventCostIdentifier value) {
        this.eventCostIdentifier = value;
    }

}
