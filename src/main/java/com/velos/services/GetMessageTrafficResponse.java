
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMessageTrafficResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMessageTrafficResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageTraffic" type="{http://velos.com/services/}messageTraffic" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMessageTrafficResponse", propOrder = {
    "messageTraffic"
})
public class GetMessageTrafficResponse {

    @XmlElement(name = "MessageTraffic")
    protected MessageTraffic messageTraffic;

    /**
     * Gets the value of the messageTraffic property.
     * 
     * @return
     *     possible object is
     *     {@link MessageTraffic }
     *     
     */
    public MessageTraffic getMessageTraffic() {
        return messageTraffic;
    }

    /**
     * Sets the value of the messageTraffic property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageTraffic }
     *     
     */
    public void setMessageTraffic(MessageTraffic value) {
        this.messageTraffic = value;
    }

}
