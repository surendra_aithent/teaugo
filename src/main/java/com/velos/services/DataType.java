
package com.velos.services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="dataType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EDIT_BOX_TEXT"/>
 *     &lt;enumeration value="EDIT_BOX_NUMBER"/>
 *     &lt;enumeration value="EDIT_BOX_DATE"/>
 *     &lt;enumeration value="EDIT_BOX_TIME"/>
 *     &lt;enumeration value="MULTIPLE_CHOICE_LOOKUP"/>
 *     &lt;enumeration value="MULTIPLE_CHOICE_DROPDOWN"/>
 *     &lt;enumeration value="MULTIPLE_CHOICE_CHECKBOX"/>
 *     &lt;enumeration value="MULTIPLE_CHOICE_RADIO_BUTTON"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "dataType")
@XmlEnum
public enum DataType {

    EDIT_BOX_TEXT,
    EDIT_BOX_NUMBER,
    EDIT_BOX_DATE,
    EDIT_BOX_TIME,
    MULTIPLE_CHOICE_LOOKUP,
    MULTIPLE_CHOICE_DROPDOWN,
    MULTIPLE_CHOICE_CHECKBOX,
    MULTIPLE_CHOICE_RADIO_BUTTON;

    public String value() {
        return name();
    }

    public static DataType fromValue(String v) {
        return valueOf(v);
    }

}
