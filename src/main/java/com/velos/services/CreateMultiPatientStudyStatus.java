
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createMultiPatientStudyStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createMultiPatientStudyStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="patientEnrollmentDetails" type="{http://velos.com/services/}patientEnrollmentDetails" minOccurs="0"/>
 *         &lt;element name="patientIdentifier" type="{http://velos.com/services/}patientIdentifier" minOccurs="0"/>
 *         &lt;element name="studyIdentifier" type="{http://velos.com/services/}studyIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMultiPatientStudyStatus", propOrder = {
    "patientEnrollmentDetails",
    "patientIdentifier",
    "studyIdentifier"
})
public class CreateMultiPatientStudyStatus
    extends ServiceObject
{

    protected PatientEnrollmentDetails patientEnrollmentDetails;
    protected PatientIdentifier patientIdentifier;
    protected StudyIdentifier studyIdentifier;

    /**
     * Gets the value of the patientEnrollmentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public PatientEnrollmentDetails getPatientEnrollmentDetails() {
        return patientEnrollmentDetails;
    }

    /**
     * Sets the value of the patientEnrollmentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientEnrollmentDetails }
     *     
     */
    public void setPatientEnrollmentDetails(PatientEnrollmentDetails value) {
        this.patientEnrollmentDetails = value;
    }

    /**
     * Gets the value of the patientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PatientIdentifier }
     *     
     */
    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    /**
     * Sets the value of the patientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientIdentifier }
     *     
     */
    public void setPatientIdentifier(PatientIdentifier value) {
        this.patientIdentifier = value;
    }

    /**
     * Gets the value of the studyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link StudyIdentifier }
     *     
     */
    public StudyIdentifier getStudyIdentifier() {
        return studyIdentifier;
    }

    /**
     * Sets the value of the studyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link StudyIdentifier }
     *     
     */
    public void setStudyIdentifier(StudyIdentifier value) {
        this.studyIdentifier = value;
    }

}
