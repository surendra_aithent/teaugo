
package com.velos.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for scheduleEventStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleEventStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://velos.com/services/}serviceObject">
 *       &lt;sequence>
 *         &lt;element name="coverageType" type="{http://velos.com/services/}code" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonForChangeCoverType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonForChangeFDAAudit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siteOfService" type="{http://velos.com/services/}organizationIdentifier" minOccurs="0"/>
 *         &lt;element name="statusValidFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleEventStatus", propOrder = {
    "coverageType",
    "notes",
    "reasonForChangeCoverType",
    "reasonForChangeFDAAudit",
    "siteOfService",
    "statusValidFrom"
})
public class ScheduleEventStatus
    extends ServiceObject
{

    protected Code coverageType;
    protected String notes;
    protected String reasonForChangeCoverType;
    protected String reasonForChangeFDAAudit;
    protected OrganizationIdentifier siteOfService;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusValidFrom;

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCoverageType(Code value) {
        this.coverageType = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the reasonForChangeCoverType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChangeCoverType() {
        return reasonForChangeCoverType;
    }

    /**
     * Sets the value of the reasonForChangeCoverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChangeCoverType(String value) {
        this.reasonForChangeCoverType = value;
    }

    /**
     * Gets the value of the reasonForChangeFDAAudit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForChangeFDAAudit() {
        return reasonForChangeFDAAudit;
    }

    /**
     * Sets the value of the reasonForChangeFDAAudit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForChangeFDAAudit(String value) {
        this.reasonForChangeFDAAudit = value;
    }

    /**
     * Gets the value of the siteOfService property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public OrganizationIdentifier getSiteOfService() {
        return siteOfService;
    }

    /**
     * Sets the value of the siteOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifier }
     *     
     */
    public void setSiteOfService(OrganizationIdentifier value) {
        this.siteOfService = value;
    }

    /**
     * Gets the value of the statusValidFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusValidFrom() {
        return statusValidFrom;
    }

    /**
     * Sets the value of the statusValidFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusValidFrom(XMLGregorianCalendar value) {
        this.statusValidFrom = value;
    }

}
