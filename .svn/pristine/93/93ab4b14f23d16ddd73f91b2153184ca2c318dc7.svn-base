<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:int="http://www.springframework.org/schema/integration"
       xmlns:ws="http://www.springframework.org/schema/integration/ws"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:camel="http://camel.apache.org/schema/spring"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
	http://camel.apache.org/schema/spring
	http://camel.apache.org/schema/spring/camel-spring.xsd
	http://camel.apache.org/schema/cxf
	http://camel.apache.org/schema/cxf/camel-cxf.xsd
    http://www.springframework.org/schema/context
    http://www.springframework.org/schema/context/spring-context-3.0.xsd
    http://www.springframework.org/schema/integration/ws
    http://www.springframework.org/schema/integration/ws/spring-integration-ws-3.0.xsd
    http://www.springframework.org/schema/integration
    http://www.springframework.org/schema/integration/spring-integration-3.0.xsd
    http://www.springframework.org/schema/util
    http://www.springframework.org/schema/util/spring-util-3.0.xsd
    ">
  <!-- We need this for loading the CXFServlet --> 
  <import resource="classpath:META-INF/cxf/cxf.xml"/>
  <!-- <context:property-placeholder location="classpath:config.properties"/> -->

  <camelContext id="camel2" xmlns="http://camel.apache.org/schema/spring">
     <route>
      <from uri="spring-ws:rootqname:{urn:ihe:qrph:rpe:2009}EnrollPatientRequestRequest?endpointMapping=#endpointMapping"/>
      <process ref="retrieveProtocolProcessor"/>
     </route>
     <route>
      <from uri="spring-ws:rootqname:{urn:ihe:qrph:rpe:2009}AlertProtocolState?endpointMapping=#endpointMapping"/>
      <process ref="retrieveProtocolProcessor"/>
    </route>
  </camelContext>
    
  <context:component-scan
      base-package="com.velos.integration.gateway"/>

  <int:channel id="inboundDOMProtocolRequest"/>

  <ws:inbound-gateway id="wsInboundGateway"
                      request-channel="inboundDOMProtocolRequest"/>

  <int:service-activator input-channel="inboundDOMProtocolRequest"
                         ref="protocolEndpoint"/>
  <bean id="retrieveProtocolProcessor" class="com.velos.integration.gateway.ProtocolEndpoint"/>
                         
  <bean id="wsServerSecurityInterceptor" class="org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor">
    <property name="validationActions" value="NoSecurity"/>
  </bean>
  
  <bean id="simpleInterceptor" class="com.velos.integration.gateway.SimpleInterceptor"></bean>
  
  
  <bean id="messageFactory" class="org.springframework.ws.soap.saaj.SaajSoapMessageFactory">
    <property name="soapVersion">
      <util:constant static-field="org.springframework.ws.soap.SoapVersion.SOAP_12"/>
    </property>
  </bean>
  
 
  <!-- === Endpoint mapping ==== -->
  <bean id="endpointMapping" class="org.apache.camel.component.spring.ws.bean.CamelEndpointMapping">
    <property name="interceptors">
      <list>
        <ref local="validatingInterceptor"/>
        <ref local="loggingInterceptor"/>
        <ref local="wsServerSecurityInterceptor"/>
        <ref local="simpleInterceptor"/>
      </list>
    </property>
  </bean>

  <!-- === Interceptors ==== -->
  <bean id="loggingInterceptor" class="org.springframework.ws.server.endpoint.interceptor.PayloadLoggingInterceptor"/>

  <bean id="validatingInterceptor"
        class="org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor">
    <property name="schemas">
      <list>
        <value>/WEB-INF/patient.xsd</value>
        <value>/WEB-INF/alertprotocol.xsd</value>
      </list> 
    </property>
    <property name="validateRequest" value="false"/>
    <property name="validateResponse" value="false"/>
  </bean>

 <bean id="patient" class="org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition">
    <property name="schema">
      <bean id="xsd2" class="org.springframework.xml.xsd.SimpleXsdSchema">
        <property name="xsd" value="/WEB-INF/patient.xsd"/>
      </bean>
    </property>
    <property name="portTypeName" value="increment"/>
    <property name="locationUri" value="http://localhost:8282/velos-patient-interface/protocolservice/patient"/>
    <property name="createSoap11Binding" value="false"/>
    <property name="createSoap12Binding" value="true"/>
  </bean>
  
   <bean id="alertprotocol" class="org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition">
    <property name="schema">
      <bean id="xsd3" class="org.springframework.xml.xsd.SimpleXsdSchema">
        <property name="xsd" value="/WEB-INF/alertprotocol.xsd"/>
      </bean>
    </property>
    <property name="portTypeName" value="increment"/>
    <property name="locationUri" value="http://localhost:8282/velos-patient-interface/protocolservice/alertprotocol"/>
    <property name="createSoap11Binding" value="false"/>
    <property name="createSoap12Binding" value="true"/>
  </bean>
 <!--  <bean id="vgdbDataSource" class="org.apache.commons.dbcp.BasicDataSource"
		destroy-method="close">

		<property name="driverClassName" value="${dataSource.driverClassName}" />
		<property name="url" value="${dataSource.url}" />
		<property name="username" value="${dataSource.username}" />
		<property name="password" value="${dataSource.password}" />
		<property name="initialSize">
			<value>5</value>
		</property>
		<property name="maxActive">
			<value>10</value>
		</property>
		<property name="maxIdle">
			<value>5</value>
		</property>
	</bean>
	
	<bean id="messageDao" class="com.velos.integration.dao.MessageDAO">
		<property name="vgdbDataSource" ref="vgdbDataSource"></property>
	</bean>
	
	<bean id="messageDAOHelper" class="com.velos.integration.dao.MessageDAOHelper">
	<property name="messageDAO" ref="messageDao"></property>
	</bean>
	
 -->	<!-- <bean id="protocolEndPoint" class="com.velos.integration.gateway.ProtocolEndpoint">
	<property name="messageDao" ref="messageDao"></property>
	</bean>
	 -->
	
	<!--  <bean id="EpicEndpointClient" class="com.velos.epic.service.EpicEndpointClient">
	<property name="messageDao" ref="messageDao"></property>
	</bean> 
	
	<bean id="webServiceTemplate" class="org.springframework.ws.client.core.WebServiceTemplate">
    <constructor-arg ref="messageFactory"/>
    <property name="defaultUri" value="${epic.scheme}://${epic.host}:${epic.port}/${epic.context}"/>
  </bean>
   -->
  <!-- <bean id="messageFactory" class="org.springframework.ws.soap.saaj.SaajSoapMessageFactory">
      <property name="soapVersion">
      <util:constant static-field="org.springframework.ws.soap.SoapVersion.SOAP_12"/>
    </property>
  </bean> -->
	
</beans>